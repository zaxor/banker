package com.bank.app.domain.enumeration;

/**
 * The PaymentOrderStatus enumeration.
 */
public enum PaymentOrderStatus {
    ZADAN, IZVRSEN, ODBIJEN
}
