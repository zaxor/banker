package com.bank.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A BankAccount.
 */
@Entity
@Table(name = "bank_account")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BankAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Size(min = 10, max = 10)
	@Column(name = "account_no", length = 10)
	private String accountNo;

	@Column(name = "funds")
	private Double funds;

	@ManyToOne
	private User user;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAccountNo() {
		return accountNo;
	}

	public BankAccount accountNo(String accountNo) {
		this.accountNo = accountNo;
		return this;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public Double getFunds() {
		return funds;
	}

	public BankAccount funds(Double funds) {
		this.funds = funds;
		return this;
	}

	public void setFunds(Double funds) {
		this.funds = funds;
	}

	public User getUser() {
		return user;
	}

	public BankAccount user(User user) {
		this.user = user;
		return this;
	}

	public void setUser(User user) {
		this.user = user;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BankAccount bankAccount = (BankAccount) o;
		if (bankAccount.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), bankAccount.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "BankAccount{" +
				"id=" + getId() +
				", accountNo='" + getAccountNo() + "'" +
				", funds=" + getFunds() +
				"}";
	}
}
