package com.bank.app.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

import com.bank.app.domain.enumeration.PaymentOrderStatus;

/**
 * A PaymentOrder.
 */
@Entity
@Table(name = "payment_order")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class PaymentOrder implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
	@SequenceGenerator(name = "sequenceGenerator")
	private Long id;

	@Column(name = "funds")
	private Double funds;

	@Enumerated(EnumType.STRING)
	@Column(name = "status")
	private PaymentOrderStatus status;

	@Column(name = "insert_date")
	private Instant insertDate;

	@Column(name = "update_date")
	private Instant updateDate;

	@ManyToOne
	private BankAccount sourceAcc;

	@ManyToOne
	private BankAccount targetAcc;

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Version
	@Column(name = "version")
	private Long version;

	// jhipster-needle-entity-add-field - JHipster will add fields here, do not
	// remove
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Double getFunds() {
		return funds;
	}

	public PaymentOrder funds(Double funds) {
		this.funds = funds;
		return this;
	}

	public void setFunds(Double funds) {
		this.funds = funds;
	}

	public PaymentOrderStatus getStatus() {
		return status;
	}

	public PaymentOrder status(PaymentOrderStatus status) {
		this.status = status;
		return this;
	}

	public void setStatus(PaymentOrderStatus status) {
		this.status = status;
	}

	public Instant getInsertDate() {
		return insertDate;
	}

	public PaymentOrder insertDate(Instant insertDate) {
		this.insertDate = insertDate;
		return this;
	}

	public void setInsertDate(Instant insertDate) {
		this.insertDate = insertDate;
	}

	public Instant getUpdateDate() {
		return updateDate;
	}

	public PaymentOrder updateDate(Instant updateDate) {
		this.updateDate = updateDate;
		return this;
	}

	public void setUpdateDate(Instant updateDate) {
		this.updateDate = updateDate;
	}

	public BankAccount getSourceAcc() {
		return sourceAcc;
	}

	public PaymentOrder sourceAcc(BankAccount bankAccount) {
		this.sourceAcc = bankAccount;
		return this;
	}

	public void setSourceAcc(BankAccount bankAccount) {
		this.sourceAcc = bankAccount;
	}

	public BankAccount getTargetAcc() {
		return targetAcc;
	}

	public PaymentOrder targetAcc(BankAccount bankAccount) {
		this.targetAcc = bankAccount;
		return this;
	}

	public void setTargetAcc(BankAccount bankAccount) {
		this.targetAcc = bankAccount;
	}
	// jhipster-needle-entity-add-getters-setters - JHipster will add getters
	// and setters here, do not remove

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		PaymentOrder paymentOrder = (PaymentOrder) o;
		if (paymentOrder.getId() == null || getId() == null) {
			return false;
		}
		return Objects.equals(getId(), paymentOrder.getId());
	}

	@Override
	public int hashCode() {
		return Objects.hashCode(getId());
	}

	@Override
	public String toString() {
		return "PaymentOrder [id=" + id + ", funds=" + funds + ", status=" + status + ", insertDate=" + insertDate
				+ ", updateDate=" + updateDate + ", sourceAcc=" + sourceAcc + ", targetAcc=" + targetAcc + ", version="
				+ version + "]";
	}
}
