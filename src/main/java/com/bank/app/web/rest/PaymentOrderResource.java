package com.bank.app.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.bank.app.service.PaymentOrderService;
import com.bank.app.web.rest.errors.BadRequestAlertException;
import com.bank.app.web.rest.util.HeaderUtil;
import com.bank.app.web.rest.util.PaginationUtil;
import com.bank.app.service.dto.PaymentOrderDTO;
import com.bank.app.service.dto.PaymentOrderCriteria;
import com.bank.app.security.SecurityUtils;
import com.bank.app.service.PaymentOrderQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing PaymentOrder.
 */
@RestController
@RequestMapping("/api")
public class PaymentOrderResource {

	private final Logger log = LoggerFactory.getLogger(PaymentOrderResource.class);

	private static final String ENTITY_NAME = "paymentOrder";

	private final PaymentOrderService paymentOrderService;

	private final PaymentOrderQueryService paymentOrderQueryService;

	public PaymentOrderResource(PaymentOrderService paymentOrderService,
			PaymentOrderQueryService paymentOrderQueryService) {
		this.paymentOrderService = paymentOrderService;
		this.paymentOrderQueryService = paymentOrderQueryService;
	}

	/**
	 * POST /payment-orders : Create a new paymentOrder.
	 *
	 * @param paymentOrderDTO
	 *            the paymentOrderDTO to create
	 * @return the ResponseEntity with status 201 (Created) and with body the
	 *         new paymentOrderDTO, or with status 400 (Bad Request) if the
	 *         paymentOrder has already an ID
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PostMapping("/payment-orders")
	@Timed
	public ResponseEntity<PaymentOrderDTO> createPaymentOrder(@RequestBody PaymentOrderDTO paymentOrderDTO)
			throws URISyntaxException {
		log.debug("REST request to save PaymentOrder : {}", paymentOrderDTO);
		if (paymentOrderDTO.getId() != null) {
			throw new BadRequestAlertException("A new paymentOrder cannot already have an ID", ENTITY_NAME, "idexists");
		}
		PaymentOrderDTO result = paymentOrderService.save(paymentOrderDTO);
		return ResponseEntity.created(new URI("/api/payment-orders/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	/**
	 * PUT /payment-orders : Updates an existing paymentOrder.
	 *
	 * @param paymentOrderDTO
	 *            the paymentOrderDTO to update
	 * @return the ResponseEntity with status 200 (OK) and with body the updated
	 *         paymentOrderDTO, or with status 400 (Bad Request) if the
	 *         paymentOrderDTO is not valid, or with status 500 (Internal Server
	 *         Error) if the paymentOrderDTO couldn't be updated
	 * @throws URISyntaxException
	 *             if the Location URI syntax is incorrect
	 */
	@PutMapping("/payment-orders")
	@Timed
	public ResponseEntity<PaymentOrderDTO> updatePaymentOrder(@RequestBody PaymentOrderDTO paymentOrderDTO)
			throws URISyntaxException {
		log.debug("REST request to update PaymentOrder : {}", paymentOrderDTO);
		if (paymentOrderDTO.getId() == null) {
			return createPaymentOrder(paymentOrderDTO);
		}
		PaymentOrderDTO result = paymentOrderService.save(paymentOrderDTO);
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, paymentOrderDTO.getId().toString()))
				.body(result);
	}

	/**
	 * GET /payment-orders : get all the paymentOrders.
	 *
	 * @param pageable
	 *            the pagination information
	 * @param criteria
	 *            the criterias which the requested entities should match
	 * @return the ResponseEntity with status 200 (OK) and the list of
	 *         paymentOrders in body
	 */
	@GetMapping("/payment-orders")
	@Timed
	public ResponseEntity<List<PaymentOrderDTO>> getAllPaymentOrders(PaymentOrderCriteria criteria, Pageable pageable) {
		log.debug("REST request to get PaymentOrders by criteria: {}", criteria);
		Page<PaymentOrderDTO> page = paymentOrderQueryService.findByCriteria(criteria, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/payment-orders");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}

	/**
	 * GET /payment-orders/:id : get the "id" paymentOrder.
	 *
	 * @param id
	 *            the id of the paymentOrderDTO to retrieve
	 * @return the ResponseEntity with status 200 (OK) and with body the
	 *         paymentOrderDTO, or with status 404 (Not Found)
	 */
	@GetMapping("/payment-orders/{id}")
	@Timed
	public ResponseEntity<PaymentOrderDTO> getPaymentOrder(@PathVariable Long id) {
		log.debug("REST request to get PaymentOrder : {}", id);
		PaymentOrderDTO paymentOrderDTO = paymentOrderService.findOne(id);
		return ResponseUtil.wrapOrNotFound(Optional.ofNullable(paymentOrderDTO));
	}

	/**
	 * DELETE /payment-orders/:id : delete the "id" paymentOrder.
	 *
	 * @param id
	 *            the id of the paymentOrderDTO to delete
	 * @return the ResponseEntity with status 200 (OK)
	 */
	@DeleteMapping("/payment-orders/{id}")
	@Timed
	public ResponseEntity<Void> deletePaymentOrder(@PathVariable Long id) {
		log.debug("REST request to delete PaymentOrder : {}", id);
		paymentOrderService.delete(id);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
	}

	@PostMapping("/payment-orders/currentuser")
	@Timed
	public ResponseEntity<PaymentOrderDTO> createPaymentOrderForCurrentUser(
			@RequestBody PaymentOrderDTO paymentOrderDTO) throws URISyntaxException {
		log.debug("REST request to save PaymentOrder for current user : {}", paymentOrderDTO);
		if (paymentOrderDTO.getId() != null) {
			throw new BadRequestAlertException("A new paymentOrder cannot already have an ID", ENTITY_NAME, "idexists");
		}
		PaymentOrderDTO result;
		try {
			result = paymentOrderService.createForUser(paymentOrderDTO, SecurityUtils.getCurrentUserLogin().get());
		} catch (IllegalArgumentException | IllegalStateException | NullPointerException e) {
			throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
		}
		return ResponseEntity.created(new URI("/api/payment-orders/" + result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	@PostMapping("/payment-orders/execute/{id}")
	@Timed
	public ResponseEntity<PaymentOrderDTO> execute(@PathVariable Long id) {
		log.debug("REST request to execute PaymentOrder : {}", id);
		PaymentOrderDTO result;
		try {
			result = paymentOrderService.findOne(id);
			if (result != null) {
				result = paymentOrderService.execute(result);
			}
		} catch (IllegalArgumentException | IllegalStateException | NullPointerException e) {
			throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
		}
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}

	@PostMapping("/payment-orders/cancel/{id}")
	@Timed
	public ResponseEntity<PaymentOrderDTO> cancel(@PathVariable Long id) {
		log.debug("REST request to cancel PaymentOrder : {}", id);
		PaymentOrderDTO result;
		try {
			result = paymentOrderService.findOne(id);
			if (result != null) {
				result = paymentOrderService.cancel(result);
			}
		} catch (IllegalArgumentException | IllegalStateException | NullPointerException e) {
			throw new BadRequestAlertException(e.getMessage(), ENTITY_NAME, e.getMessage());
		}
		return ResponseEntity.ok()
				.headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}
}
