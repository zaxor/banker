package com.bank.app.repository;

import com.bank.app.domain.PaymentOrder;
import org.springframework.stereotype.Repository;

import javax.persistence.LockModeType;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data JPA repository for the PaymentOrder entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PaymentOrderRepository
		extends JpaRepository<PaymentOrder, Long>, JpaSpecificationExecutor<PaymentOrder> {

	@Query("select p from PaymentOrder p where p.id = :id")
	@Lock(LockModeType.OPTIMISTIC)
	PaymentOrder lockFindOneById(@Param("id") Long id);

}
