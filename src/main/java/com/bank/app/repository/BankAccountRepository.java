package com.bank.app.repository;

import com.bank.app.domain.BankAccount;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;

import java.util.List;

import javax.persistence.LockModeType;

/**
 * Spring Data JPA repository for the BankAccount entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BankAccountRepository extends JpaRepository<BankAccount, Long>, JpaSpecificationExecutor<BankAccount> {

	@Query("select bank_account from BankAccount bank_account where bank_account.user.login = ?#{principal.username}")
	List<BankAccount> findByUserIsCurrentUser();

	@Query(value = "select nextval('account_no_seq')", nativeQuery = true)
	Long getNextAccountNo();

	BankAccount findOneByUserId(Long id);

	BankAccount findOneByAccountNo(String accountNo);

	@Lock(LockModeType.PESSIMISTIC_WRITE)
	@Query("select b from BankAccount b where b.id = :id")
	BankAccount lockFindOneById(@Param("id") Long id);
}
