package com.bank.app.aop.payment;

import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.bank.app.service.PaymentOrderService;
import com.bank.app.service.dto.PaymentOrderDTO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class PaymentPostProcessingAspect {

	private final PaymentOrderService paymentOrderService;

	@Value("${payment.autoExecuteLimit}")
	private Double autoExecuteLimit = 100d;

	@Value("${payment.skipLimit}")
	private Integer skipLimit = 10;

	private int skipCount = 1;

	@Pointcut("execution(* com.bank.app.service.impl.PaymentOrderServiceImpl.createForUser(com.bank.app.service.dto.PaymentOrderDTO, String))")
	public void paymentCreatedPointcut() {

	}

	@AfterReturning(pointcut = "paymentCreatedPointcut()", returning = "retVal")
	public void attemptExecute(Object retVal) {
		log.debug("Post processing payment order: '{}'.", retVal);
		PaymentOrderDTO paymentOrderDTO = (PaymentOrderDTO) retVal;
		if (paymentOrderDTO.getFunds() <= autoExecuteLimit) {
			paymentOrderDTO = paymentOrderService.findOne(paymentOrderDTO.getId());
			if (checkSkipCount()) {
				paymentOrderService.execute(paymentOrderDTO);
			}
		}
	}

	private boolean checkSkipCount() {
		if (skipCount == skipLimit) {
			skipCount = 1;
			return false;
		}

		skipCount++;
		return true;
	}
}
