package com.bank.app.aop.registration;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bank.app.domain.User;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.service.BankAccountService;
import com.bank.app.service.dto.UserDTO;
import com.bank.app.service.ext.BankAccountServiceExt;
import com.bank.app.service.mapper.UserMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
@RequiredArgsConstructor
public class BankAccountAspect {
	private final BankAccountServiceExt bankAccountServiceExt;
	private final UserMapper userMapper;

	@Pointcut("execution(* com.bank.app.service.UserService.registerUser(com.bank.app.service.dto.UserDTO, String))")
	public void userRegisteredPointCut() {

	}

	@Around("userRegisteredPointCut()")
	@Transactional
	public Object aroundUserRegistered(ProceedingJoinPoint jointPoint)
			throws Throwable {
		log.debug("Bank acc creating aspect invoked.");
		try {
			Object result = jointPoint.proceed();
			if (result instanceof User) {
				log.debug("Creating bank acc for user '{}'.", ((User) result).getLogin());
				this.bankAccountServiceExt.createFromUser(userMapper.userToUserDTO((User) result));
			}

			return result;
		} catch (Throwable e) {
			log.error("An error occured in aspect method. Bank account was not created.", e);
			throw e;
		}
	}
}
