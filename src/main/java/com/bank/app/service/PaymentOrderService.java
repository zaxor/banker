package com.bank.app.service;

import com.bank.app.service.dto.PaymentOrderDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing PaymentOrder.
 */
public interface PaymentOrderService {

    /**
     * Save a paymentOrder.
     *
     * @param paymentOrderDTO the entity to save
     * @return the persisted entity
     */
    PaymentOrderDTO save(PaymentOrderDTO paymentOrderDTO);

    /**
     * Get all the paymentOrders.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<PaymentOrderDTO> findAll(Pageable pageable);

    /**
     * Get the "id" paymentOrder.
     *
     * @param id the id of the entity
     * @return the entity
     */
    PaymentOrderDTO findOne(Long id);

    /**
     * Delete the "id" paymentOrder.
     *
     * @param id the id of the entity
     */
    void delete(Long id);

	PaymentOrderDTO createForUser(PaymentOrderDTO paymentOrderDTO, String login);

	PaymentOrderDTO cancel(PaymentOrderDTO paymentOrderDTO);

	PaymentOrderDTO execute(PaymentOrderDTO paymentOrderDTO);
}
