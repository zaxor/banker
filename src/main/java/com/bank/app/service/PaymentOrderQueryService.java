package com.bank.app.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.bank.app.domain.PaymentOrder;
import com.bank.app.domain.*; // for static metamodels
import com.bank.app.repository.PaymentOrderRepository;
import com.bank.app.service.dto.PaymentOrderCriteria;

import com.bank.app.service.dto.PaymentOrderDTO;
import com.bank.app.service.mapper.PaymentOrderMapper;
import com.bank.app.domain.enumeration.PaymentOrderStatus;

/**
 * Service for executing complex queries for PaymentOrder entities in the database.
 * The main input is a {@link PaymentOrderCriteria} which get's converted to {@link Specifications},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link PaymentOrderDTO} or a {@link Page} of {@link PaymentOrderDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PaymentOrderQueryService extends QueryService<PaymentOrder> {

    private final Logger log = LoggerFactory.getLogger(PaymentOrderQueryService.class);


    private final PaymentOrderRepository paymentOrderRepository;

    private final PaymentOrderMapper paymentOrderMapper;

    public PaymentOrderQueryService(PaymentOrderRepository paymentOrderRepository, PaymentOrderMapper paymentOrderMapper) {
        this.paymentOrderRepository = paymentOrderRepository;
        this.paymentOrderMapper = paymentOrderMapper;
    }

    /**
     * Return a {@link List} of {@link PaymentOrderDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<PaymentOrderDTO> findByCriteria(PaymentOrderCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specifications<PaymentOrder> specification = createSpecification(criteria);
        return paymentOrderMapper.toDto(paymentOrderRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link PaymentOrderDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<PaymentOrderDTO> findByCriteria(PaymentOrderCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specifications<PaymentOrder> specification = createSpecification(criteria);
        final Page<PaymentOrder> result = paymentOrderRepository.findAll(specification, page);
        return result.map(paymentOrderMapper::toDto);
    }

    /**
     * Function to convert PaymentOrderCriteria to a {@link Specifications}
     */
    private Specifications<PaymentOrder> createSpecification(PaymentOrderCriteria criteria) {
        Specifications<PaymentOrder> specification = Specifications.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), PaymentOrder_.id));
            }
            if (criteria.getFunds() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFunds(), PaymentOrder_.funds));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getStatus(), PaymentOrder_.status));
            }
            if (criteria.getInsertDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getInsertDate(), PaymentOrder_.insertDate));
            }
            if (criteria.getUpdateDate() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getUpdateDate(), PaymentOrder_.updateDate));
            }
            if (criteria.getSourceAccId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getSourceAccId(), PaymentOrder_.sourceAcc, BankAccount_.id));
            }
            if (criteria.getTargetAccId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getTargetAccId(), PaymentOrder_.targetAcc, BankAccount_.id));
            }
        }
        return specification;
    }

}
