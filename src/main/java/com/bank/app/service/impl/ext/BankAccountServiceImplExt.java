package com.bank.app.service.impl.ext;

import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bank.app.domain.BankAccount;
import com.bank.app.domain.User;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.service.dto.BankAccountDTO;
import com.bank.app.service.dto.UserDTO;
import com.bank.app.service.ext.BankAccountServiceExt;
import com.bank.app.service.mapper.BankAccountMapper;
import com.bank.app.service.mapper.UserMapper;

import lombok.RequiredArgsConstructor;

@Service
@Transactional
@RequiredArgsConstructor
public class BankAccountServiceImplExt implements BankAccountServiceExt {

	private final BankAccountRepository bankAccountRepository;
	private final BankAccountMapper bankAccountMapper;
	private final UserMapper userMapper;

	@Value("${bankAccount.defaultFunds}")
	private Double defaultFunds;

	@Override
	public BankAccountDTO createFromUser(UserDTO userDTO) {

		Objects.requireNonNull(userDTO);
		Objects.requireNonNull(userDTO.getId());

		BankAccount acc = new BankAccount();
		acc.setFunds(defaultFunds);
		acc.setUser(userMapper.userFromId(userDTO.getId()));
		acc.setAccountNo(formatAccountNo(bankAccountRepository.getNextAccountNo()));

		return bankAccountMapper.toDto(bankAccountRepository.save(acc));
	}

	String formatAccountNo(Long accountNo) {
		return accountNo != null ? StringUtils.leftPad(accountNo.toString(), 10, "0") : null;
	}
}
