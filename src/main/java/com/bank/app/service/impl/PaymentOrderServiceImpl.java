package com.bank.app.service.impl;

import com.bank.app.service.PaymentOrderService;
import com.bank.app.service.UserService;
import com.bank.app.domain.BankAccount;
import com.bank.app.domain.PaymentOrder;
import com.bank.app.domain.User;
import com.bank.app.domain.enumeration.PaymentOrderStatus;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.repository.PaymentOrderRepository;
import com.bank.app.repository.UserRepository;
import com.bank.app.service.dto.PaymentOrderDTO;
import com.bank.app.service.mapper.PaymentOrderMapper;

import afu.org.checkerframework.checker.units.qual.s;
import lombok.RequiredArgsConstructor;

import java.time.Instant;
import java.util.Objects;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing PaymentOrder.
 */
@Service
@Transactional
@RequiredArgsConstructor
public class PaymentOrderServiceImpl implements PaymentOrderService {

	private final Logger log = LoggerFactory.getLogger(PaymentOrderServiceImpl.class);

	private final PaymentOrderRepository paymentOrderRepository;

	private final PaymentOrderMapper paymentOrderMapper;

	private final UserRepository userRepository;

	private final BankAccountRepository bankAccountRepository;

	/**
	 * Save a paymentOrder.
	 *
	 * @param paymentOrderDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public PaymentOrderDTO save(PaymentOrderDTO paymentOrderDTO) {
		log.debug("Request to save PaymentOrder : {}", paymentOrderDTO);
		PaymentOrder paymentOrder = paymentOrderMapper.toEntity(paymentOrderDTO);
		paymentOrder = paymentOrderRepository.save(paymentOrder);
		return paymentOrderMapper.toDto(paymentOrder);
	}

	/**
	 * Get all the paymentOrders.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<PaymentOrderDTO> findAll(Pageable pageable) {
		log.debug("Request to get all PaymentOrders");
		return paymentOrderRepository.findAll(pageable)
				.map(paymentOrderMapper::toDto);
	}

	/**
	 * Get one paymentOrder by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public PaymentOrderDTO findOne(Long id) {
		log.debug("Request to get PaymentOrder : {}", id);
		PaymentOrder paymentOrder = paymentOrderRepository.findOne(id);
		return paymentOrderMapper.toDto(paymentOrder);
	}

	/**
	 * Delete the paymentOrder by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete PaymentOrder : {}", id);
		paymentOrderRepository.delete(id);
	}

	@Override
	public PaymentOrderDTO createForUser(PaymentOrderDTO paymentOrderDTO, String login) {

		Objects.requireNonNull(paymentOrderDTO);
		Objects.requireNonNull(paymentOrderDTO.getTargetAccId());
		Objects.requireNonNull(login);

		if (paymentOrderDTO.getFunds() <= 0) {
			throw new IllegalArgumentException("The field funds is in wrong format.");
		}

		Optional<User> user = userRepository.findOneByLogin(login);
		if (!user.isPresent()) {
			throw new IllegalArgumentException("The user does not exist.");
		}

		BankAccount sourceAccount = bankAccountRepository.findOneByUserId(user.get().getId());
		if (sourceAccount == null) {
			throw new IllegalArgumentException("The user does not have a bank account set.");
		}

		if (!bankAccountRepository.exists(paymentOrderDTO.getTargetAccId())) {
			throw new IllegalArgumentException("The target account does not exist.");
		}

		if (sourceAccount.getFunds() - paymentOrderDTO.getFunds() < 0) {
			throw new IllegalArgumentException("Insufficient funds.");
		}

		paymentOrderDTO.setSourceAccId(sourceAccount.getId());
		paymentOrderDTO.setTargetAccId(paymentOrderDTO.getTargetAccId());
		paymentOrderDTO.setStatus(PaymentOrderStatus.ZADAN);
		paymentOrderDTO.setInsertDate(Instant.now());
		paymentOrderDTO.setUpdateDate(paymentOrderDTO.getInsertDate());

		return this.save(paymentOrderDTO);
	}

	@Override
	public PaymentOrderDTO execute(PaymentOrderDTO paymentOrderDTO) {
		
		log.debug("Executing payment order with id '{}' getting source account.", paymentOrderDTO.getId());
		BankAccount sourceAcc = bankAccountRepository.lockFindOneById(paymentOrderDTO.getSourceAccId());
		log.debug("Executing payment order with id '{}' getting target account.", paymentOrderDTO.getId());
		BankAccount targetAcc = bankAccountRepository.lockFindOneById(paymentOrderDTO.getTargetAccId());

		log.debug("Executing payment order with id '{}'.", paymentOrderDTO.getId());
		PaymentOrder paymentOrder = paymentOrderRepository.lockFindOneById(paymentOrderDTO.getId());

		if (!paymentOrder.getStatus().equals(PaymentOrderStatus.ZADAN)) {
			return paymentOrderMapper.toDto(paymentOrder);
		}

		if (paymentOrder.getFunds() <= 0) {
			throw new IllegalStateException("Payment order funds in wrong format.");
		}

		if (sourceAcc.getFunds() - paymentOrder.getFunds() < 0) {
			log.debug("Executing payment order with id '{}'. Insufficient funds, refusing.", paymentOrderDTO.getId());
			paymentOrder.setStatus(PaymentOrderStatus.ODBIJEN);
		} else {
			sourceAcc.setFunds(sourceAcc.getFunds() - paymentOrder.getFunds());
			targetAcc.setFunds(targetAcc.getFunds() + paymentOrder.getFunds());
			paymentOrder.setStatus(PaymentOrderStatus.IZVRSEN);

			log.debug("Executing payment order with id '{}'. Source funds: '{}'.", paymentOrder.getId(),
					sourceAcc.getFunds());
			log.debug("Executing payment order with id '{}'. Target funds: '{}'.", paymentOrder.getId(),
					targetAcc.getFunds());

			bankAccountRepository.save(sourceAcc);
			bankAccountRepository.save(targetAcc);
		}

		paymentOrder.setUpdateDate(Instant.now());
		log.debug("Executing payment order with id '{}'. Saving.", paymentOrder.getId());
		return paymentOrderMapper.toDto(paymentOrderRepository.saveAndFlush(paymentOrder));
	}

	@Override
	public PaymentOrderDTO cancel(PaymentOrderDTO paymentOrderDTO) {
		PaymentOrder paymentOrder = paymentOrderRepository.lockFindOneById(paymentOrderDTO.getId());
		paymentOrder.setStatus(PaymentOrderStatus.ODBIJEN);
		paymentOrder.setUpdateDate(Instant.now());

		return paymentOrderMapper.toDto(paymentOrderRepository.save(paymentOrder));
	}
}
