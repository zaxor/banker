package com.bank.app.service.impl;

import com.bank.app.service.BankAccountService;
import com.bank.app.domain.BankAccount;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.service.dto.BankAccountDTO;
import com.bank.app.service.mapper.BankAccountMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing BankAccount.
 */
@Service
@Transactional
public class BankAccountServiceImpl implements BankAccountService {

	private final Logger log = LoggerFactory.getLogger(BankAccountServiceImpl.class);

	private final BankAccountRepository bankAccountRepository;

	private final BankAccountMapper bankAccountMapper;

	public BankAccountServiceImpl(BankAccountRepository bankAccountRepository, BankAccountMapper bankAccountMapper) {
		this.bankAccountRepository = bankAccountRepository;
		this.bankAccountMapper = bankAccountMapper;
	}

	/**
	 * Save a bankAccount.
	 *
	 * @param bankAccountDTO
	 *            the entity to save
	 * @return the persisted entity
	 */
	@Override
	public BankAccountDTO save(BankAccountDTO bankAccountDTO) {
		log.debug("Request to save BankAccount : {}", bankAccountDTO);
		BankAccount bankAccount = bankAccountMapper.toEntity(bankAccountDTO);
		bankAccount = bankAccountRepository.save(bankAccount);
		return bankAccountMapper.toDto(bankAccount);
	}

	/**
	 * Get all the bankAccounts.
	 *
	 * @param pageable
	 *            the pagination information
	 * @return the list of entities
	 */
	@Override
	@Transactional(readOnly = true)
	public Page<BankAccountDTO> findAll(Pageable pageable) {
		log.debug("Request to get all BankAccounts");
		return bankAccountRepository.findAll(pageable)
				.map(bankAccountMapper::toDto);
	}

	/**
	 * Get one bankAccount by id.
	 *
	 * @param id
	 *            the id of the entity
	 * @return the entity
	 */
	@Override
	@Transactional(readOnly = true)
	public BankAccountDTO findOne(Long id) {
		log.debug("Request to get BankAccount : {}", id);
		BankAccount bankAccount = bankAccountRepository.findOne(id);
		return bankAccountMapper.toDto(bankAccount);
	}

	/**
	 * Delete the bankAccount by id.
	 *
	 * @param id
	 *            the id of the entity
	 */
	@Override
	public void delete(Long id) {
		log.debug("Request to delete BankAccount : {}", id);
		bankAccountRepository.delete(id);
	}
}
