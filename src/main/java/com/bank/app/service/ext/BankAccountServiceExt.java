package com.bank.app.service.ext;

import com.bank.app.service.dto.BankAccountDTO;
import com.bank.app.service.dto.UserDTO;

public interface BankAccountServiceExt {

	BankAccountDTO createFromUser(UserDTO userDTO);

}
