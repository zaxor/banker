package com.bank.app.service.dto;

import java.io.Serializable;
import com.bank.app.domain.enumeration.PaymentOrderStatus;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

import io.github.jhipster.service.filter.InstantFilter;




/**
 * Criteria class for the PaymentOrder entity. This class is used in PaymentOrderResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /payment-orders?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PaymentOrderCriteria implements Serializable {
    /**
     * Class for filtering PaymentOrderStatus
     */
    public static class PaymentOrderStatusFilter extends Filter<PaymentOrderStatus> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private DoubleFilter funds;

    private PaymentOrderStatusFilter status;

    private InstantFilter insertDate;

    private InstantFilter updateDate;

    private LongFilter sourceAccId;

    private LongFilter targetAccId;

    public PaymentOrderCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public DoubleFilter getFunds() {
        return funds;
    }

    public void setFunds(DoubleFilter funds) {
        this.funds = funds;
    }

    public PaymentOrderStatusFilter getStatus() {
        return status;
    }

    public void setStatus(PaymentOrderStatusFilter status) {
        this.status = status;
    }

    public InstantFilter getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(InstantFilter insertDate) {
        this.insertDate = insertDate;
    }

    public InstantFilter getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(InstantFilter updateDate) {
        this.updateDate = updateDate;
    }

    public LongFilter getSourceAccId() {
        return sourceAccId;
    }

    public void setSourceAccId(LongFilter sourceAccId) {
        this.sourceAccId = sourceAccId;
    }

    public LongFilter getTargetAccId() {
        return targetAccId;
    }

    public void setTargetAccId(LongFilter targetAccId) {
        this.targetAccId = targetAccId;
    }

    @Override
    public String toString() {
        return "PaymentOrderCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (funds != null ? "funds=" + funds + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (insertDate != null ? "insertDate=" + insertDate + ", " : "") +
                (updateDate != null ? "updateDate=" + updateDate + ", " : "") +
                (sourceAccId != null ? "sourceAccId=" + sourceAccId + ", " : "") +
                (targetAccId != null ? "targetAccId=" + targetAccId + ", " : "") +
            "}";
    }

}
