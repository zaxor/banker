package com.bank.app.service.dto;


import java.time.Instant;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;
import com.bank.app.domain.enumeration.PaymentOrderStatus;

/**
 * A DTO for the PaymentOrder entity.
 */
public class PaymentOrderDTO implements Serializable {

    private Long id;

    private Double funds;

    private PaymentOrderStatus status;

    private Instant insertDate;

    private Instant updateDate;

    private Long sourceAccId;

    private String sourceAccAccountNo;

    private Long targetAccId;

    private String targetAccAccountNo;

	public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getFunds() {
        return funds;
    }

    public void setFunds(Double funds) {
        this.funds = funds;
    }

    public PaymentOrderStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentOrderStatus status) {
        this.status = status;
    }

    public Instant getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Instant insertDate) {
        this.insertDate = insertDate;
    }

    public Instant getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Instant updateDate) {
        this.updateDate = updateDate;
    }

    public Long getSourceAccId() {
        return sourceAccId;
    }

    public void setSourceAccId(Long bankAccountId) {
        this.sourceAccId = bankAccountId;
    }

    public String getSourceAccAccountNo() {
        return sourceAccAccountNo;
    }

    public void setSourceAccAccountNo(String bankAccountAccountNo) {
        this.sourceAccAccountNo = bankAccountAccountNo;
    }

    public Long getTargetAccId() {
        return targetAccId;
    }

    public void setTargetAccId(Long bankAccountId) {
        this.targetAccId = bankAccountId;
    }

    public String getTargetAccAccountNo() {
        return targetAccAccountNo;
    }

    public void setTargetAccAccountNo(String bankAccountAccountNo) {
        this.targetAccAccountNo = bankAccountAccountNo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PaymentOrderDTO paymentOrderDTO = (PaymentOrderDTO) o;
        if(paymentOrderDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), paymentOrderDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
	public String toString() {
		return "PaymentOrderDTO [id=" + id + ", funds=" + funds + ", status=" + status + ", insertDate=" + insertDate
				+ ", updateDate=" + updateDate + ", sourceAccId=" + sourceAccId + ", sourceAccAccountNo="
				+ sourceAccAccountNo + ", targetAccId=" + targetAccId + ", targetAccAccountNo=" + targetAccAccountNo
				+ "]";
	}
}
