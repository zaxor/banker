package com.bank.app.service.mapper;

import com.bank.app.domain.*;
import com.bank.app.service.dto.PaymentOrderDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity PaymentOrder and its DTO PaymentOrderDTO.
 */
@Mapper(componentModel = "spring", uses = { BankAccountMapper.class })
public interface PaymentOrderMapper extends EntityMapper<PaymentOrderDTO, PaymentOrder> {

	@Mapping(source = "sourceAcc.id", target = "sourceAccId")
	@Mapping(source = "sourceAcc.accountNo", target = "sourceAccAccountNo")
	@Mapping(source = "targetAcc.id", target = "targetAccId")
	@Mapping(source = "targetAcc.accountNo", target = "targetAccAccountNo")
	PaymentOrderDTO toDto(PaymentOrder paymentOrder);

	@Mapping(source = "sourceAccId", target = "sourceAcc")
	@Mapping(source = "targetAccId", target = "targetAcc")
	PaymentOrder toEntity(PaymentOrderDTO paymentOrderDTO);

	default PaymentOrder fromId(Long id) {
		if (id == null) {
			return null;
		}
		PaymentOrder paymentOrder = new PaymentOrder();
		paymentOrder.setId(id);
		return paymentOrder;
	}
}
