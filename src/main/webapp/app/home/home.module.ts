import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BankerSharedModule } from '../shared';

import { HOME_ROUTE, HomeComponent } from './';
import { UserActionsModule } from '../user-actions/user-actions.module';
import { AdminActionsModule } from '../admin-actions/admin-actions.module';

@NgModule({
    imports: [
        BankerSharedModule,
        RouterModule.forChild([ HOME_ROUTE ]),
        UserActionsModule,
        AdminActionsModule
    ],
    declarations: [
        HomeComponent,
    ],
    entryComponents: [
    ],
    providers: [
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankerHomeModule {}
