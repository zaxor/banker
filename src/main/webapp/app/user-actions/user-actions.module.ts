import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { PaymentDialogComponent, PaymentDialogPopupComponent } from './payment-dialog/payment-dialog.component';
import { PaymentComponent } from './payment/payment.component';
import { paymentPopupRoute } from './user-actions.route';
import { RouterModule } from '@angular/router';
import { BankerSharedModule } from '../shared';
import { PaymentOrderNgResolvePagingParams, PaymentOrderNgService, PaymentOrderNgPopupService } from '../entities/payment-order-ng';

const ENTITY_STATES = [
    ...paymentPopupRoute
];

@NgModule({
    imports: [
        CommonModule,
        BankerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        UserPanelComponent,
        PaymentDialogComponent,
        PaymentDialogPopupComponent,
        PaymentComponent],
    entryComponents: [
        UserPanelComponent,
        PaymentDialogComponent,
        PaymentDialogPopupComponent,
        PaymentComponent],
    providers: [
        PaymentOrderNgService,
        PaymentOrderNgPopupService,
        PaymentOrderNgResolvePagingParams],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    exports: [UserPanelComponent]
})
export class UserActionsModule { }
