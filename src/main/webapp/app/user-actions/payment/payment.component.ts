import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaymentOrderNg, PaymentOrderNgService } from '../../entities/payment-order-ng';
import { Subscription } from 'rxjs';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Principal, ITEMS_PER_PAGE } from '../../shared';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { BankAccountNgService, BankAccountNg } from '../../entities/bank-account-ng';

@Component({
    selector: 'jhi-payment',
    templateUrl: './payment.component.html',
    styles: []
})
export class PaymentComponent implements OnInit, OnDestroy {
    currentAccount: any;
    paymentOrders: PaymentOrderNg[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    currentBankAccount: BankAccountNg;
    paymentOrderQuery: PaymentOrderNg = new PaymentOrderNg();

    constructor(
        private paymentOrderService: PaymentOrderNgService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private bankAccountService: BankAccountNgService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = 1;
            // this.previousPage = data.pagingParams.page || 1;
            this.reverse = true;
            this.predicate = 'insertDate';
        });
    }

    search() {
        this.page = 1;
        this.loadAll();
    }

    loadAll() {

        const req = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort(),
            'sourceAccId.equals': this.currentBankAccount.id
        };

        if (this.paymentOrderQuery.status) {
            req['status.equals'] = this.paymentOrderQuery.status;
        }

        this.paymentOrderService.query(req).subscribe(
            (res: HttpResponse<PaymentOrderNg[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment-order-ng'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/payment-order-ng', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.principal.identity().then((account) => {
            this.currentAccount = account;
            this.loadCurrentBankAccount();
        });
    }

    private loadCurrentBankAccount() {
        if (this.currentAccount) {
            this.bankAccountService.query({
                page: 0,
                size: 1,
                'userId.equals': this.currentAccount.id
            }).subscribe(
                (res: HttpResponse<BankAccountNg[]>) => {
                    this.currentBankAccount = res.body[0];
                    this.loadAll();
                    this.registerChangeInPaymentOrders();
                },
                (res: HttpErrorResponse) => this.onError(res.message)
            );
        }
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PaymentOrderNg) {
        return item.id;
    }
    registerChangeInPaymentOrders() {
        this.eventSubscriber = this.eventManager.subscribe('paymentOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.paymentOrders = data;
    }
    private onError(error) {
        this.jhiAlertService.error('error.message', { message: error.errorKey }, null);
    }
}
