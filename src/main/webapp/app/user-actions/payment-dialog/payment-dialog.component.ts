import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { UserService, Principal } from '../../shared';
import { BankAccountNgService, BankAccountNg } from '../../entities/bank-account-ng';
import { PaymentOrderNgService, PaymentOrderNg, PaymentOrderNgPopupService } from '../../entities/payment-order-ng';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';

@Component({
    selector: 'jhi-payment-dialog',
    templateUrl: './payment-dialog.component.html',
    styles: []
})
export class PaymentDialogComponent implements OnInit {

    paymentOrder: PaymentOrderNg;
    isSaving: boolean;

    bankaccounts: BankAccountNg[];
    currentBankAccount: BankAccountNg;

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private bankAccountService: BankAccountNgService,
        private paymentOrderService: PaymentOrderNgService,
        private eventManager: JhiEventManager,
        private principal: Principal,
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.bankAccountService.query()
            .subscribe((res: HttpResponse<BankAccountNg[]>) => {
                this.principal.identity().then((user) => {
                    this.bankaccounts = res.body;
                    this.currentBankAccount = this.bankaccounts.find((x) => x.userId === user.id);
                    this.bankaccounts = this.bankaccounts.filter((x) => x.userId !== user.id);
                });

            }, (res: HttpErrorResponse) => this.onError(res.message));

        this.paymentOrder.funds = 1;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;

        this.subscribeToSaveResponse(
            this.paymentOrderService.createForCurrentUser(this.paymentOrder));
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PaymentOrderNg>>) {
        result.subscribe((res: HttpResponse<PaymentOrderNg>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError(res.error));
    }

    private onSaveSuccess(result: PaymentOrderNg) {
        this.eventManager.broadcast({ name: 'paymentOrderListModification', content: 'OK' });
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError(error) {
        this.isSaving = false;
        this.jhiAlertService.error('error.message', { message: error.errorKey }, null);
    }

    private onError(error) {
        // this.jhiAlertService.error(error.message, null, null);
    }

    trackBankAccountById(index: number, item: BankAccountNg) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-payment-dialog-popup',
    template: ''
})
export class PaymentDialogPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentOrderPopupService: PaymentOrderNgPopupService
    ) { }

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {

            this.paymentOrderPopupService
                .open(PaymentDialogComponent as Component);

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
