import { Component, OnInit, OnDestroy } from '@angular/core';
import { PaymentOrderNg, PaymentOrderNgService, PaymentOrderStatus } from '../../entities/payment-order-ng';
import { Subscription } from 'rxjs';
import { JhiParseLinks, JhiAlertService, JhiEventManager } from 'ng-jhipster';
import { Principal, ITEMS_PER_PAGE } from '../../shared';
import { ActivatedRoute, Router } from '@angular/router';
import { BankAccountNgService } from '../../entities/bank-account-ng';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

@Component({
    selector: 'jhi-admin-payment-order',
    templateUrl: './admin-payment-order.component.html',
    styles: []
})
export class AdminPaymentOrderComponent implements OnInit, OnDestroy {

    paymentOrders: PaymentOrderNg[];
    error: any;
    success: any;
    eventSubscriber: Subscription;
    routeData: any;
    links: any;
    totalItems: any;
    queryCount: any;
    itemsPerPage: any;
    page: any;
    predicate: any;
    previousPage: any;
    reverse: any;
    paymentOrderQuery: PaymentOrderNg = new PaymentOrderNg();

    constructor(
        private paymentOrderService: PaymentOrderNgService,
        private parseLinks: JhiParseLinks,
        private jhiAlertService: JhiAlertService,
        private principal: Principal,
        private activatedRoute: ActivatedRoute,
        private router: Router,
        private eventManager: JhiEventManager,
        private bankAccountService: BankAccountNgService
    ) {
        this.itemsPerPage = ITEMS_PER_PAGE;
        this.routeData = this.activatedRoute.data.subscribe((data) => {
            this.page = 1;
            // this.previousPage = data.pagingParams.page || 1;
            this.reverse = true;
            this.predicate = 'insertDate';
        });
    }

    search() {
        this.page = 1;
        this.loadAll();
    }

    loadAll() {

        const req = {
            page: this.page - 1,
            size: this.itemsPerPage,
            sort: this.sort(),
        };

        if (this.paymentOrderQuery.status) {
            req['status.equals'] = this.paymentOrderQuery.status;
        }

        this.paymentOrderService.query(req).subscribe(
            (res: HttpResponse<PaymentOrderNg[]>) => this.onSuccess(res.body, res.headers),
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    loadPage(page: number) {
        if (page !== this.previousPage) {
            this.previousPage = page;
            this.transition();
        }
    }
    transition() {
        this.router.navigate(['/payment-order-ng'], {
            queryParams:
                {
                    page: this.page,
                    size: this.itemsPerPage,
                    sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
                }
        });
        this.loadAll();
    }

    clear() {
        this.page = 0;
        this.router.navigate(['/payment-order-ng', {
            page: this.page,
            sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
        }]);
        this.loadAll();
    }
    ngOnInit() {
        this.loadAll();
        this.registerChangeInPaymentOrders();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: PaymentOrderNg) {
        return item.id;
    }
    registerChangeInPaymentOrders() {
        this.eventSubscriber = this.eventManager.subscribe('paymentOrderListModification', (response) => this.loadAll());
    }

    sort() {
        const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
        if (this.predicate !== 'id') {
            result.push('id');
        }
        return result;
    }

    private onSuccess(data, headers) {
        this.links = this.parseLinks.parse(headers.get('link'));
        this.totalItems = headers.get('X-Total-Count');
        this.queryCount = this.totalItems;
        // this.page = pagingParams.page;
        this.paymentOrders = data;
    }
    private onError(error) {
        if (error.errorKey) {
            this.jhiAlertService.error('error.message', { message: error.errorKey }, null);
        } else {
            this.jhiAlertService.error(error.message, null, null);
        }

    }

    checkStatus(paymentOrder: PaymentOrderNg) {
        return (!paymentOrder.status) || (paymentOrder.status.toString() !== 'ZADAN');
    }

    execute(paymentOrder: PaymentOrderNg) {
        const copyStatus = paymentOrder.status;
        paymentOrder.status = null;

        this.paymentOrderService.execute(paymentOrder.id).subscribe(
            (res: HttpResponse<PaymentOrderNg>) => { console.log(res); this.onExecuteSuccess(paymentOrder, res.body, res.headers); },
            (res: HttpErrorResponse) => {
                paymentOrder.status = copyStatus;
                this.onError(res.message);
            }
        );
    }

    cancel(paymentOrder: PaymentOrderNg) {
        const copyStatus = paymentOrder.status;
        paymentOrder.status = null;
        this.paymentOrderService.cancel(paymentOrder.id).subscribe(
            (res: HttpResponse<PaymentOrderNg>) => { console.log(res); this.onExecuteSuccess(paymentOrder, res.body, res.headers); },
            (res: HttpErrorResponse) => {
                paymentOrder.status = copyStatus;
                this.onError(res.message);
            }
        );
    }

    private onExecuteSuccess(entity: PaymentOrderNg, data: PaymentOrderNg, headers) {
        entity.status = data.status;
        entity.updateDate = data.updateDate;
    }
}
