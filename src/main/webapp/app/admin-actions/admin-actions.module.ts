import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminPanelComponent } from './admin-panel/admin-panel.component';
import { AdminPaymentOrderComponent } from './admin-payment-order/admin-payment-order.component';
import { BankerSharedModule } from '../shared';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        CommonModule,
        BankerSharedModule,
        RouterModule.forChild([])
    ],
    declarations: [AdminPanelComponent, AdminPaymentOrderComponent],
    exports: [AdminPanelComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AdminActionsModule { }
