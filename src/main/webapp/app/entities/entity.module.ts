import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { BankerBankAccountNgModule } from './bank-account-ng/bank-account-ng.module';
import { BankerPaymentOrderNgModule } from './payment-order-ng/payment-order-ng.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        BankerBankAccountNgModule,
        BankerPaymentOrderNgModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankerEntityModule {}
