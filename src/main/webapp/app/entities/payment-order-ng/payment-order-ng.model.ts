import { BaseEntity } from './../../shared';

export const enum PaymentOrderStatus {
    'ZADAN',
    'IZVRSEN',
    'ODBIJEN'
}

export class PaymentOrderNg implements BaseEntity {
    constructor(
        public id?: number,
        public funds?: number,
        public status?: PaymentOrderStatus,
        public insertDate?: any,
        public updateDate?: any,
        public sourceAccId?: number,
        public targetAccId?: number,
    ) {
    }
}
