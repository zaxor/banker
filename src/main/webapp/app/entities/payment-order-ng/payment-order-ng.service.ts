import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { PaymentOrderNg } from './payment-order-ng.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<PaymentOrderNg>;

@Injectable()
export class PaymentOrderNgService {

    private resourceUrl = SERVER_API_URL + 'api/payment-orders';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(paymentOrder: PaymentOrderNg): Observable<EntityResponseType> {
        const copy = this.convert(paymentOrder);
        return this.http.post<PaymentOrderNg>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(paymentOrder: PaymentOrderNg): Observable<EntityResponseType> {
        const copy = this.convert(paymentOrder);
        return this.http.put<PaymentOrderNg>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<PaymentOrderNg>(`${this.resourceUrl}/${id}`, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<PaymentOrderNg[]>> {
        const options = createRequestOption(req);
        return this.http.get<PaymentOrderNg[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<PaymentOrderNg[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
    }

    createForCurrentUser(paymentOrder: PaymentOrderNg): Observable<EntityResponseType> {
        const copy = this.convert(paymentOrder);
        return this.http.post<PaymentOrderNg>(this.resourceUrl + '/currentuser', copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    execute(id: number): Observable<EntityResponseType> {
        return this.http.post<PaymentOrderNg>(`${this.resourceUrl}/execute/${id}`, null, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    cancel(id: number): Observable<EntityResponseType> {
        return this.http.post<PaymentOrderNg>(`${this.resourceUrl}/cancel/${id}`, null, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: PaymentOrderNg = this.convertItemFromServer(res.body);
        return res.clone({ body });
    }

    private convertArrayResponse(res: HttpResponse<PaymentOrderNg[]>): HttpResponse<PaymentOrderNg[]> {
        const jsonResponse: PaymentOrderNg[] = res.body;
        const body: PaymentOrderNg[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({ body });
    }

    /**
     * Convert a returned JSON object to PaymentOrderNg.
     */
    private convertItemFromServer(paymentOrder: PaymentOrderNg): PaymentOrderNg {
        const copy: PaymentOrderNg = Object.assign({}, paymentOrder);
        copy.insertDate = this.dateUtils
            .convertDateTimeFromServer(paymentOrder.insertDate);
        copy.updateDate = this.dateUtils
            .convertDateTimeFromServer(paymentOrder.updateDate);
        return copy;
    }

    /**
     * Convert a PaymentOrderNg to a JSON which can be sent to the server.
     */
    private convert(paymentOrder: PaymentOrderNg): PaymentOrderNg {
        const copy: PaymentOrderNg = Object.assign({}, paymentOrder);

        copy.insertDate = this.dateUtils.toDate(paymentOrder.insertDate);

        copy.updateDate = this.dateUtils.toDate(paymentOrder.updateDate);
        return copy;
    }
}
