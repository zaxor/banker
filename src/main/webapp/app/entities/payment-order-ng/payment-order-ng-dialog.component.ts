import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { PaymentOrderNg } from './payment-order-ng.model';
import { PaymentOrderNgPopupService } from './payment-order-ng-popup.service';
import { PaymentOrderNgService } from './payment-order-ng.service';
import { BankAccountNg, BankAccountNgService } from '../bank-account-ng';

@Component({
    selector: 'jhi-payment-order-ng-dialog',
    templateUrl: './payment-order-ng-dialog.component.html'
})
export class PaymentOrderNgDialogComponent implements OnInit {

    paymentOrder: PaymentOrderNg;
    isSaving: boolean;

    bankaccounts: BankAccountNg[];

    constructor(
        public activeModal: NgbActiveModal,
        private jhiAlertService: JhiAlertService,
        private paymentOrderService: PaymentOrderNgService,
        private bankAccountService: BankAccountNgService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
        this.bankAccountService.query()
            .subscribe((res: HttpResponse<BankAccountNg[]>) => { this.bankaccounts = res.body; }, (res: HttpErrorResponse) => this.onError(res.message));
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.paymentOrder.id !== undefined) {
            this.subscribeToSaveResponse(
                this.paymentOrderService.update(this.paymentOrder));
        } else {
            this.subscribeToSaveResponse(
                this.paymentOrderService.create(this.paymentOrder));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<PaymentOrderNg>>) {
        result.subscribe((res: HttpResponse<PaymentOrderNg>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: PaymentOrderNg) {
        this.eventManager.broadcast({ name: 'paymentOrderListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(error: any) {
        this.jhiAlertService.error(error.message, null, null);
    }

    trackBankAccountById(index: number, item: BankAccountNg) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-payment-order-ng-popup',
    template: ''
})
export class PaymentOrderNgPopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentOrderPopupService: PaymentOrderNgPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.paymentOrderPopupService
                    .open(PaymentOrderNgDialogComponent as Component, params['id']);
            } else {
                this.paymentOrderPopupService
                    .open(PaymentOrderNgDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
