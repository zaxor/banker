import { Injectable, Component } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpResponse } from '@angular/common/http';
import { DatePipe } from '@angular/common';
import { PaymentOrderNg } from './payment-order-ng.model';
import { PaymentOrderNgService } from './payment-order-ng.service';

@Injectable()
export class PaymentOrderNgPopupService {
    private ngbModalRef: NgbModalRef;

    constructor(
        private datePipe: DatePipe,
        private modalService: NgbModal,
        private router: Router,
        private paymentOrderService: PaymentOrderNgService

    ) {
        this.ngbModalRef = null;
    }

    open(component: Component, id?: number | any): Promise<NgbModalRef> {
        return new Promise<NgbModalRef>((resolve, reject) => {
            const isOpen = this.ngbModalRef !== null;
            if (isOpen) {
                resolve(this.ngbModalRef);
            }

            if (id) {
                this.paymentOrderService.find(id)
                    .subscribe((paymentOrderResponse: HttpResponse<PaymentOrderNg>) => {
                        const paymentOrder: PaymentOrderNg = paymentOrderResponse.body;
                        paymentOrder.insertDate = this.datePipe
                            .transform(paymentOrder.insertDate, 'yyyy-MM-ddTHH:mm:ss');
                        paymentOrder.updateDate = this.datePipe
                            .transform(paymentOrder.updateDate, 'yyyy-MM-ddTHH:mm:ss');
                        this.ngbModalRef = this.paymentOrderModalRef(component, paymentOrder);
                        resolve(this.ngbModalRef);
                    });
            } else {
                // setTimeout used as a workaround for getting ExpressionChangedAfterItHasBeenCheckedError
                setTimeout(() => {
                    this.ngbModalRef = this.paymentOrderModalRef(component, new PaymentOrderNg());
                    resolve(this.ngbModalRef);
                }, 0);
            }
        });
    }

    paymentOrderModalRef(component: Component, paymentOrder: PaymentOrderNg): NgbModalRef {
        const modalRef = this.modalService.open(component, { size: 'lg', backdrop: 'static'});
        modalRef.componentInstance.paymentOrder = paymentOrder;
        modalRef.result.then((result) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        }, (reason) => {
            this.router.navigate([{ outlets: { popup: null }}], { replaceUrl: true, queryParamsHandling: 'merge' });
            this.ngbModalRef = null;
        });
        return modalRef;
    }
}
