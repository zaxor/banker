import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { PaymentOrderNgComponent } from './payment-order-ng.component';
import { PaymentOrderNgDetailComponent } from './payment-order-ng-detail.component';
import { PaymentOrderNgPopupComponent } from './payment-order-ng-dialog.component';
import { PaymentOrderNgDeletePopupComponent } from './payment-order-ng-delete-dialog.component';

@Injectable()
export class PaymentOrderNgResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const paymentOrderRoute: Routes = [
    {
        path: 'payment-order-ng',
        component: PaymentOrderNgComponent,
        resolve: {
            'pagingParams': PaymentOrderNgResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.paymentOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'payment-order-ng/:id',
        component: PaymentOrderNgDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.paymentOrder.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const paymentOrderPopupRoute: Routes = [
    {
        path: 'payment-order-ng-new',
        component: PaymentOrderNgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.paymentOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-order-ng/:id/edit',
        component: PaymentOrderNgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.paymentOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'payment-order-ng/:id/delete',
        component: PaymentOrderNgDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.paymentOrder.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
