import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentOrderNg } from './payment-order-ng.model';
import { PaymentOrderNgService } from './payment-order-ng.service';

@Component({
    selector: 'jhi-payment-order-ng-detail',
    templateUrl: './payment-order-ng-detail.component.html'
})
export class PaymentOrderNgDetailComponent implements OnInit, OnDestroy {

    paymentOrder: PaymentOrderNg;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private paymentOrderService: PaymentOrderNgService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInPaymentOrders();
    }

    load(id) {
        this.paymentOrderService.find(id)
            .subscribe((paymentOrderResponse: HttpResponse<PaymentOrderNg>) => {
                this.paymentOrder = paymentOrderResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInPaymentOrders() {
        this.eventSubscriber = this.eventManager.subscribe(
            'paymentOrderListModification',
            (response) => this.load(this.paymentOrder.id)
        );
    }
}
