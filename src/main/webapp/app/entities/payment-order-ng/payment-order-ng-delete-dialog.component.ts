import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { PaymentOrderNg } from './payment-order-ng.model';
import { PaymentOrderNgPopupService } from './payment-order-ng-popup.service';
import { PaymentOrderNgService } from './payment-order-ng.service';

@Component({
    selector: 'jhi-payment-order-ng-delete-dialog',
    templateUrl: './payment-order-ng-delete-dialog.component.html'
})
export class PaymentOrderNgDeleteDialogComponent {

    paymentOrder: PaymentOrderNg;

    constructor(
        private paymentOrderService: PaymentOrderNgService,
        public activeModal: NgbActiveModal,
        private eventManager: JhiEventManager
    ) {
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    confirmDelete(id: number) {
        this.paymentOrderService.delete(id).subscribe((response) => {
            this.eventManager.broadcast({
                name: 'paymentOrderListModification',
                content: 'Deleted an paymentOrder'
            });
            this.activeModal.dismiss(true);
        });
    }
}

@Component({
    selector: 'jhi-payment-order-ng-delete-popup',
    template: ''
})
export class PaymentOrderNgDeletePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private paymentOrderPopupService: PaymentOrderNgPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            this.paymentOrderPopupService
                .open(PaymentOrderNgDeleteDialogComponent as Component, params['id']);
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
