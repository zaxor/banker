import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BankerSharedModule } from '../../shared';
import {
    PaymentOrderNgService,
    PaymentOrderNgPopupService,
    PaymentOrderNgComponent,
    PaymentOrderNgDetailComponent,
    PaymentOrderNgDialogComponent,
    PaymentOrderNgPopupComponent,
    PaymentOrderNgDeletePopupComponent,
    PaymentOrderNgDeleteDialogComponent,
    paymentOrderRoute,
    paymentOrderPopupRoute,
    PaymentOrderNgResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...paymentOrderRoute,
    ...paymentOrderPopupRoute,
];

@NgModule({
    imports: [
        BankerSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        PaymentOrderNgComponent,
        PaymentOrderNgDetailComponent,
        PaymentOrderNgDialogComponent,
        PaymentOrderNgDeleteDialogComponent,
        PaymentOrderNgPopupComponent,
        PaymentOrderNgDeletePopupComponent,
    ],
    entryComponents: [
        PaymentOrderNgComponent,
        PaymentOrderNgDialogComponent,
        PaymentOrderNgPopupComponent,
        PaymentOrderNgDeleteDialogComponent,
        PaymentOrderNgDeletePopupComponent,
    ],
    providers: [
        PaymentOrderNgService,
        PaymentOrderNgPopupService,
        PaymentOrderNgResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankerPaymentOrderNgModule {}
