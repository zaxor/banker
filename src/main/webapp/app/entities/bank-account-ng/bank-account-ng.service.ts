import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { BankAccountNg } from './bank-account-ng.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<BankAccountNg>;

@Injectable()
export class BankAccountNgService {

    private resourceUrl =  SERVER_API_URL + 'api/bank-accounts';

    constructor(private http: HttpClient) { }

    create(bankAccount: BankAccountNg): Observable<EntityResponseType> {
        const copy = this.convert(bankAccount);
        return this.http.post<BankAccountNg>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(bankAccount: BankAccountNg): Observable<EntityResponseType> {
        const copy = this.convert(bankAccount);
        return this.http.put<BankAccountNg>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<BankAccountNg>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<BankAccountNg[]>> {
        const options = createRequestOption(req);
        return this.http.get<BankAccountNg[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<BankAccountNg[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: BankAccountNg = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<BankAccountNg[]>): HttpResponse<BankAccountNg[]> {
        const jsonResponse: BankAccountNg[] = res.body;
        const body: BankAccountNg[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to BankAccountNg.
     */
    private convertItemFromServer(bankAccount: BankAccountNg): BankAccountNg {
        const copy: BankAccountNg = Object.assign({}, bankAccount);
        return copy;
    }

    /**
     * Convert a BankAccountNg to a JSON which can be sent to the server.
     */
    private convert(bankAccount: BankAccountNg): BankAccountNg {
        const copy: BankAccountNg = Object.assign({}, bankAccount);
        return copy;
    }
}
