import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil } from 'ng-jhipster';

import { UserRouteAccessService } from '../../shared';
import { BankAccountNgComponent } from './bank-account-ng.component';
import { BankAccountNgDetailComponent } from './bank-account-ng-detail.component';
import { BankAccountNgPopupComponent } from './bank-account-ng-dialog.component';
import { BankAccountNgDeletePopupComponent } from './bank-account-ng-delete-dialog.component';

@Injectable()
export class BankAccountNgResolvePagingParams implements Resolve<any> {

    constructor(private paginationUtil: JhiPaginationUtil) {}

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const page = route.queryParams['page'] ? route.queryParams['page'] : '1';
        const sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
        return {
            page: this.paginationUtil.parsePage(page),
            predicate: this.paginationUtil.parsePredicate(sort),
            ascending: this.paginationUtil.parseAscending(sort)
      };
    }
}

export const bankAccountRoute: Routes = [
    {
        path: 'bank-account-ng',
        component: BankAccountNgComponent,
        resolve: {
            'pagingParams': BankAccountNgResolvePagingParams
        },
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.bankAccount.home.title'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'bank-account-ng/:id',
        component: BankAccountNgDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.bankAccount.home.title'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const bankAccountPopupRoute: Routes = [
    {
        path: 'bank-account-ng-new',
        component: BankAccountNgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.bankAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bank-account-ng/:id/edit',
        component: BankAccountNgPopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.bankAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'bank-account-ng/:id/delete',
        component: BankAccountNgDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'bankerApp.bankAccount.home.title'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
