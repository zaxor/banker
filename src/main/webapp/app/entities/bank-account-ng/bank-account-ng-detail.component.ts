import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { BankAccountNg } from './bank-account-ng.model';
import { BankAccountNgService } from './bank-account-ng.service';

@Component({
    selector: 'jhi-bank-account-ng-detail',
    templateUrl: './bank-account-ng-detail.component.html'
})
export class BankAccountNgDetailComponent implements OnInit, OnDestroy {

    bankAccount: BankAccountNg;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private bankAccountService: BankAccountNgService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInBankAccounts();
    }

    load(id) {
        this.bankAccountService.find(id)
            .subscribe((bankAccountResponse: HttpResponse<BankAccountNg>) => {
                this.bankAccount = bankAccountResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInBankAccounts() {
        this.eventSubscriber = this.eventManager.subscribe(
            'bankAccountListModification',
            (response) => this.load(this.bankAccount.id)
        );
    }
}
