import { BaseEntity } from './../../shared';

export class BankAccountNg implements BaseEntity {
    constructor(
        public id?: number,
        public accountNo?: string,
        public funds?: number,
        public userId?: number,
    ) {
    }
}
