import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BankerSharedModule } from '../../shared';
import { BankerAdminModule } from '../../admin/admin.module';
import {
    BankAccountNgService,
    BankAccountNgPopupService,
    BankAccountNgComponent,
    BankAccountNgDetailComponent,
    BankAccountNgDialogComponent,
    BankAccountNgPopupComponent,
    BankAccountNgDeletePopupComponent,
    BankAccountNgDeleteDialogComponent,
    bankAccountRoute,
    bankAccountPopupRoute,
    BankAccountNgResolvePagingParams,
} from './';

const ENTITY_STATES = [
    ...bankAccountRoute,
    ...bankAccountPopupRoute,
];

@NgModule({
    imports: [
        BankerSharedModule,
        BankerAdminModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        BankAccountNgComponent,
        BankAccountNgDetailComponent,
        BankAccountNgDialogComponent,
        BankAccountNgDeleteDialogComponent,
        BankAccountNgPopupComponent,
        BankAccountNgDeletePopupComponent,
    ],
    entryComponents: [
        BankAccountNgComponent,
        BankAccountNgDialogComponent,
        BankAccountNgPopupComponent,
        BankAccountNgDeleteDialogComponent,
        BankAccountNgDeletePopupComponent,
    ],
    providers: [
        BankAccountNgService,
        BankAccountNgPopupService,
        BankAccountNgResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BankerBankAccountNgModule {}
