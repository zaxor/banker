/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BankerTestModule } from '../../../test.module';
import { PaymentOrderNgDialogComponent } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng-dialog.component';
import { PaymentOrderNgService } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.service';
import { PaymentOrderNg } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.model';
import { BankAccountNgService } from '../../../../../../main/webapp/app/entities/bank-account-ng';

describe('Component Tests', () => {

    describe('PaymentOrderNg Management Dialog Component', () => {
        let comp: PaymentOrderNgDialogComponent;
        let fixture: ComponentFixture<PaymentOrderNgDialogComponent>;
        let service: PaymentOrderNgService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [PaymentOrderNgDialogComponent],
                providers: [
                    BankAccountNgService,
                    PaymentOrderNgService
                ]
            })
            .overrideTemplate(PaymentOrderNgDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PaymentOrderNgDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentOrderNgService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('save', () => {
            it('Should call update service on save for existing entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PaymentOrderNg(123);
                        spyOn(service, 'update').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.paymentOrder = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.update).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'paymentOrderListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );

            it('Should call create service on save for new entity',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        const entity = new PaymentOrderNg();
                        spyOn(service, 'create').and.returnValue(Observable.of(new HttpResponse({body: entity})));
                        comp.paymentOrder = entity;
                        // WHEN
                        comp.save();
                        tick(); // simulate async

                        // THEN
                        expect(service.create).toHaveBeenCalledWith(entity);
                        expect(comp.isSaving).toEqual(false);
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalledWith({ name: 'paymentOrderListModification', content: 'OK'});
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
