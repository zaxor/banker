/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BankerTestModule } from '../../../test.module';
import { PaymentOrderNgDetailComponent } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng-detail.component';
import { PaymentOrderNgService } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.service';
import { PaymentOrderNg } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.model';

describe('Component Tests', () => {

    describe('PaymentOrderNg Management Detail Component', () => {
        let comp: PaymentOrderNgDetailComponent;
        let fixture: ComponentFixture<PaymentOrderNgDetailComponent>;
        let service: PaymentOrderNgService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [PaymentOrderNgDetailComponent],
                providers: [
                    PaymentOrderNgService
                ]
            })
            .overrideTemplate(PaymentOrderNgDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PaymentOrderNgDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentOrderNgService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new PaymentOrderNg(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.paymentOrder).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
