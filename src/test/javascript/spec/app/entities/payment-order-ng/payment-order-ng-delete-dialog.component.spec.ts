/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BankerTestModule } from '../../../test.module';
import { PaymentOrderNgDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng-delete-dialog.component';
import { PaymentOrderNgService } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.service';

describe('Component Tests', () => {

    describe('PaymentOrderNg Management Delete Component', () => {
        let comp: PaymentOrderNgDeleteDialogComponent;
        let fixture: ComponentFixture<PaymentOrderNgDeleteDialogComponent>;
        let service: PaymentOrderNgService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [PaymentOrderNgDeleteDialogComponent],
                providers: [
                    PaymentOrderNgService
                ]
            })
            .overrideTemplate(PaymentOrderNgDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PaymentOrderNgDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentOrderNgService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
