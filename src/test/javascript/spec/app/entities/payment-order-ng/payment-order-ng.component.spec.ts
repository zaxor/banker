/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BankerTestModule } from '../../../test.module';
import { PaymentOrderNgComponent } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.component';
import { PaymentOrderNgService } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.service';
import { PaymentOrderNg } from '../../../../../../main/webapp/app/entities/payment-order-ng/payment-order-ng.model';

describe('Component Tests', () => {

    describe('PaymentOrderNg Management Component', () => {
        let comp: PaymentOrderNgComponent;
        let fixture: ComponentFixture<PaymentOrderNgComponent>;
        let service: PaymentOrderNgService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [PaymentOrderNgComponent],
                providers: [
                    PaymentOrderNgService
                ]
            })
            .overrideTemplate(PaymentOrderNgComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(PaymentOrderNgComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(PaymentOrderNgService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new PaymentOrderNg(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.paymentOrders[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
