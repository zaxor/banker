/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BankerTestModule } from '../../../test.module';
import { BankAccountNgComponent } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.component';
import { BankAccountNgService } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.service';
import { BankAccountNg } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.model';

describe('Component Tests', () => {

    describe('BankAccountNg Management Component', () => {
        let comp: BankAccountNgComponent;
        let fixture: ComponentFixture<BankAccountNgComponent>;
        let service: BankAccountNgService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [BankAccountNgComponent],
                providers: [
                    BankAccountNgService
                ]
            })
            .overrideTemplate(BankAccountNgComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountNgComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountNgService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new BankAccountNg(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.bankAccounts[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
