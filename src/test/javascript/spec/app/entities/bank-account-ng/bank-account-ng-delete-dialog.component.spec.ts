/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { BankerTestModule } from '../../../test.module';
import { BankAccountNgDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng-delete-dialog.component';
import { BankAccountNgService } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.service';

describe('Component Tests', () => {

    describe('BankAccountNg Management Delete Component', () => {
        let comp: BankAccountNgDeleteDialogComponent;
        let fixture: ComponentFixture<BankAccountNgDeleteDialogComponent>;
        let service: BankAccountNgService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [BankAccountNgDeleteDialogComponent],
                providers: [
                    BankAccountNgService
                ]
            })
            .overrideTemplate(BankAccountNgDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountNgDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountNgService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
