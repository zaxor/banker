/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { BankerTestModule } from '../../../test.module';
import { BankAccountNgDetailComponent } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng-detail.component';
import { BankAccountNgService } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.service';
import { BankAccountNg } from '../../../../../../main/webapp/app/entities/bank-account-ng/bank-account-ng.model';

describe('Component Tests', () => {

    describe('BankAccountNg Management Detail Component', () => {
        let comp: BankAccountNgDetailComponent;
        let fixture: ComponentFixture<BankAccountNgDetailComponent>;
        let service: BankAccountNgService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [BankerTestModule],
                declarations: [BankAccountNgDetailComponent],
                providers: [
                    BankAccountNgService
                ]
            })
            .overrideTemplate(BankAccountNgDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(BankAccountNgDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(BankAccountNgService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new BankAccountNg(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.bankAccount).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
