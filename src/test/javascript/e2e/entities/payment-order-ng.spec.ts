import { browser, element, by } from 'protractor';
import { NavBarPage } from './../page-objects/jhi-page-objects';

describe('PaymentOrder e2e test', () => {

    let navBarPage: NavBarPage;
    let paymentOrderDialogPage: PaymentOrderDialogPage;
    let paymentOrderComponentsPage: PaymentOrderComponentsPage;

    beforeAll(() => {
        browser.get('/');
        browser.waitForAngular();
        navBarPage = new NavBarPage();
        navBarPage.getSignInPage().autoSignInUsing('admin', 'admin');
        browser.waitForAngular();
    });

    it('should load PaymentOrders', () => {
        navBarPage.goToEntity('payment-order-ng');
        paymentOrderComponentsPage = new PaymentOrderComponentsPage();
        expect(paymentOrderComponentsPage.getTitle())
            .toMatch(/bankerApp.paymentOrder.home.title/);

    });

    it('should load create PaymentOrder dialog', () => {
        paymentOrderComponentsPage.clickOnCreateButton();
        paymentOrderDialogPage = new PaymentOrderDialogPage();
        expect(paymentOrderDialogPage.getModalTitle())
            .toMatch(/bankerApp.paymentOrder.home.createOrEditLabel/);
        paymentOrderDialogPage.close();
    });

    it('should create and save PaymentOrders', () => {
        paymentOrderComponentsPage.clickOnCreateButton();
        paymentOrderDialogPage.setFundsInput('5');
        expect(paymentOrderDialogPage.getFundsInput()).toMatch('5');
        paymentOrderDialogPage.statusSelectLastOption();
        paymentOrderDialogPage.setInsertDateInput(12310020012301);
        expect(paymentOrderDialogPage.getInsertDateInput()).toMatch('2001-12-31T02:30');
        paymentOrderDialogPage.setUpdateDateInput(12310020012301);
        expect(paymentOrderDialogPage.getUpdateDateInput()).toMatch('2001-12-31T02:30');
        paymentOrderDialogPage.sourceAccSelectLastOption();
        paymentOrderDialogPage.targetAccSelectLastOption();
        paymentOrderDialogPage.save();
        expect(paymentOrderDialogPage.getSaveButton().isPresent()).toBeFalsy();
    });

    afterAll(() => {
        navBarPage.autoSignOut();
    });
});

export class PaymentOrderComponentsPage {
    createButton = element(by.css('.jh-create-entity'));
    title = element.all(by.css('jhi-payment-order-ng div h2 span')).first();

    clickOnCreateButton() {
        return this.createButton.click();
    }

    getTitle() {
        return this.title.getAttribute('jhiTranslate');
    }
}

export class PaymentOrderDialogPage {
    modalTitle = element(by.css('h4#myPaymentOrderLabel'));
    saveButton = element(by.css('.modal-footer .btn.btn-primary'));
    closeButton = element(by.css('button.close'));
    fundsInput = element(by.css('input#field_funds'));
    statusSelect = element(by.css('select#field_status'));
    insertDateInput = element(by.css('input#field_insertDate'));
    updateDateInput = element(by.css('input#field_updateDate'));
    sourceAccSelect = element(by.css('select#field_sourceAcc'));
    targetAccSelect = element(by.css('select#field_targetAcc'));

    getModalTitle() {
        return this.modalTitle.getAttribute('jhiTranslate');
    }

    setFundsInput = function(funds) {
        this.fundsInput.sendKeys(funds);
    };

    getFundsInput = function() {
        return this.fundsInput.getAttribute('value');
    };

    setStatusSelect = function(status) {
        this.statusSelect.sendKeys(status);
    };

    getStatusSelect = function() {
        return this.statusSelect.element(by.css('option:checked')).getText();
    };

    statusSelectLastOption = function() {
        this.statusSelect.all(by.tagName('option')).last().click();
    };
    setInsertDateInput = function(insertDate) {
        this.insertDateInput.sendKeys(insertDate);
    };

    getInsertDateInput = function() {
        return this.insertDateInput.getAttribute('value');
    };

    setUpdateDateInput = function(updateDate) {
        this.updateDateInput.sendKeys(updateDate);
    };

    getUpdateDateInput = function() {
        return this.updateDateInput.getAttribute('value');
    };

    sourceAccSelectLastOption = function() {
        this.sourceAccSelect.all(by.tagName('option')).last().click();
    };

    sourceAccSelectOption = function(option) {
        this.sourceAccSelect.sendKeys(option);
    };

    getSourceAccSelect = function() {
        return this.sourceAccSelect;
    };

    getSourceAccSelectedOption = function() {
        return this.sourceAccSelect.element(by.css('option:checked')).getText();
    };

    targetAccSelectLastOption = function() {
        this.targetAccSelect.all(by.tagName('option')).last().click();
    };

    targetAccSelectOption = function(option) {
        this.targetAccSelect.sendKeys(option);
    };

    getTargetAccSelect = function() {
        return this.targetAccSelect;
    };

    getTargetAccSelectedOption = function() {
        return this.targetAccSelect.element(by.css('option:checked')).getText();
    };

    save() {
        this.saveButton.click();
    }

    close() {
        this.closeButton.click();
    }

    getSaveButton() {
        return this.saveButton;
    }
}
