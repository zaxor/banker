package com.bank.app.service.impl.ext;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bank.app.BankerApp;
import com.bank.app.service.dto.BankAccountDTO;
import com.bank.app.service.dto.UserDTO;
import com.bank.app.service.ext.BankAccountServiceExt;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankerApp.class)
@Transactional
@Slf4j
public class BankAccountServiceImplExtIntTest {

	private static final Long userId = 1L;

	private Double defaultFunds = 1000d;
	private int accountNoSize = 10;

	@Autowired
	private BankAccountServiceExt bankAccountServiceExt;

	@Test
	@Transactional
	public void testCreateFromUser() {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(userId);

		BankAccountDTO actual = bankAccountServiceExt.createFromUser(userDTO);
		assertNotNull(actual);
		assertNotNull(actual.getId());
		assertEquals(userId, actual.getUserId());

		log.debug("Account no is '{}'.", actual.getAccountNo());
		assertThat(actual.getAccountNo()).isNotNull();
		assertThat(actual.getAccountNo()).hasSize(accountNoSize);
		assertThat(actual.getFunds()).isEqualTo(defaultFunds);
	}

}
