package com.bank.app.service.impl;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.bank.app.BankerApp;
import com.bank.app.domain.BankAccount;
import com.bank.app.domain.PaymentOrder;
import com.bank.app.domain.enumeration.PaymentOrderStatus;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.repository.PaymentOrderRepository;
import com.bank.app.service.PaymentOrderService;
import com.bank.app.service.dto.PaymentOrderDTO;
import com.bank.app.service.mapper.PaymentOrderMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankerApp.class)
public class PaymentOrderServiceImplTest {

	private static double fundRate = 1d;
	private static int nThreads = 100;
	private static final ExecutorService pool = Executors.newFixedThreadPool(nThreads);

	@Autowired
	private PaymentOrderRepository paymentOrderRepository;

	@Autowired
	private BankAccountRepository bankAccountRepository;

	@Autowired
	private PaymentOrderService paymentOrderService;

	@Autowired
	private PaymentOrderMapper paymentOrderMapper;

	private Long lastId;

	@Before
	public void setUp() {
		lastId = this.paymentOrderRepository.findAll().stream()
				.max((x, y) -> Long.compare(x.getId(), y.getId()))
				.map(x -> x.getId())
				.get();
	}

	@After
	public void tearDown() {
		List<PaymentOrder> list = this.paymentOrderRepository.findAll().stream()
				.filter(x -> x.getId() > lastId)
				.collect(Collectors.toList());
		this.paymentOrderRepository.delete(list);
	}

	@Test
	public void testConcurrentExecutesSerially() throws InterruptedException {
		BankAccount sourceAcc = new BankAccount();
		sourceAcc.setId(1L);
		BankAccount targetAcc = new BankAccount();
		targetAcc.setId(2L);

		List<TestExecutor> tasks = new ArrayList<>(nThreads);
		for (int i = 0; i < nThreads; i++) {

			PaymentOrder paymentOrder = new PaymentOrder()
					.funds(fundRate)
					.insertDate(Instant.now())
					.updateDate(Instant.now())
					.sourceAcc(sourceAcc)
					.targetAcc(targetAcc)
					.status(PaymentOrderStatus.ZADAN);
			paymentOrder = paymentOrderRepository.save(paymentOrder);

			tasks.add(new TestExecutor(paymentOrderMapper.toDto(paymentOrder)));
		}

		Double startFundsSource = bankAccountRepository.findOne(1L).getFunds();
		Double startFundsTarget = bankAccountRepository.findOne(2L).getFunds();

		pool.invokeAll(tasks);
		pool.shutdown();
		pool.awaitTermination(1, TimeUnit.MINUTES);

		BankAccount sourceAccAfter = bankAccountRepository.findOne(1L);
		BankAccount targetAccAfter = bankAccountRepository.findOne(2L);

		Double expectedDiff = nThreads * fundRate;
		assertThat(startFundsSource).isEqualTo(sourceAccAfter.getFunds() + expectedDiff);
		assertThat(startFundsTarget).isEqualTo(targetAccAfter.getFunds() - expectedDiff);
	}

	private class TestExecutor implements Callable<Void> {

		private final PaymentOrderDTO paymentOrderDTO;

		public TestExecutor(PaymentOrderDTO paymentOrderDTO) {
			super();
			this.paymentOrderDTO = paymentOrderDTO;
		}

		@Override
		public Void call() throws Exception {
			paymentOrderService.execute(paymentOrderDTO);
			return null;
		}

	}
}
