package com.bank.app.web.rest;

import com.bank.app.BankerApp;

import com.bank.app.domain.PaymentOrder;
import com.bank.app.domain.BankAccount;
import com.bank.app.domain.BankAccount;
import com.bank.app.repository.BankAccountRepository;
import com.bank.app.repository.PaymentOrderRepository;
import com.bank.app.service.PaymentOrderService;
import com.bank.app.service.dto.PaymentOrderDTO;
import com.bank.app.service.mapper.PaymentOrderMapper;
import com.bank.app.web.rest.errors.ExceptionTranslator;
import com.bank.app.service.dto.PaymentOrderCriteria;
import com.bank.app.service.PaymentOrderQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.bank.app.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.bank.app.domain.enumeration.PaymentOrderStatus;

/**
 * Test class for the PaymentOrderResource REST controller.
 *
 * @see PaymentOrderResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankerApp.class)
public class PaymentOrderResourceIntTest {

	private static final Double DEFAULT_FUNDS = 1D;
	private static final Double UPDATED_FUNDS = 2D;

	private static final PaymentOrderStatus DEFAULT_STATUS = PaymentOrderStatus.ZADAN;
	private static final PaymentOrderStatus UPDATED_STATUS = PaymentOrderStatus.IZVRSEN;

	private static final Instant DEFAULT_INSERT_DATE = Instant.ofEpochMilli(0L);
	private static final Instant UPDATED_INSERT_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

	private static final Instant DEFAULT_UPDATE_DATE = Instant.ofEpochMilli(0L);
	private static final Instant UPDATED_UPDATE_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

	@Autowired
	private PaymentOrderRepository paymentOrderRepository;

	@Autowired
	private PaymentOrderMapper paymentOrderMapper;

	@Autowired
	private PaymentOrderService paymentOrderService;

	@Autowired
	private PaymentOrderQueryService paymentOrderQueryService;

	@Autowired
	private MappingJackson2HttpMessageConverter jacksonMessageConverter;

	@Autowired
	private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

	@Autowired
	private ExceptionTranslator exceptionTranslator;

	@Autowired
	private EntityManager em;

	@Autowired
	private BankAccountRepository bankAccountRepository;

	private MockMvc restPaymentOrderMockMvc;

	private PaymentOrder paymentOrder;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final PaymentOrderResource paymentOrderResource = new PaymentOrderResource(paymentOrderService,
				paymentOrderQueryService);
		this.restPaymentOrderMockMvc = MockMvcBuilders.standaloneSetup(paymentOrderResource)
				.setCustomArgumentResolvers(pageableArgumentResolver)
				.setControllerAdvice(exceptionTranslator)
				.setConversionService(createFormattingConversionService())
				.setMessageConverters(jacksonMessageConverter).build();
	}

	/**
	 * Create an entity for this test.
	 *
	 * This is a static method, as tests for other entities might also need it,
	 * if they test an entity which requires the current entity.
	 */
	public static PaymentOrder createEntity(EntityManager em) {
		PaymentOrder paymentOrder = new PaymentOrder()
				.funds(DEFAULT_FUNDS)
				.status(DEFAULT_STATUS)
				.insertDate(DEFAULT_INSERT_DATE)
				.updateDate(DEFAULT_UPDATE_DATE);
		return paymentOrder;
	}

	@Before
	public void initTest() {
		paymentOrder = createEntity(em);
	}

	@Test
	@Transactional
	public void createPaymentOrder() throws Exception {
		int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();

		// Create the PaymentOrder
		PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);
		restPaymentOrderMockMvc.perform(post("/api/payment-orders")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isCreated());

		// Validate the PaymentOrder in the database
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate + 1);
		PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
		assertThat(testPaymentOrder.getFunds()).isEqualTo(DEFAULT_FUNDS);
		assertThat(testPaymentOrder.getStatus()).isEqualTo(DEFAULT_STATUS);
		assertThat(testPaymentOrder.getInsertDate()).isEqualTo(DEFAULT_INSERT_DATE);
		assertThat(testPaymentOrder.getUpdateDate()).isEqualTo(DEFAULT_UPDATE_DATE);
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "user", password = "user", roles = "USER")
	public void createPaymentOrderForCurrentUser() throws Exception {

		Double funds = 500d;

		int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();
		BankAccount sourceAcc = bankAccountRepository.findOneByUserId(4L);
		BankAccount targetAcc = bankAccountRepository.findOneByUserId(6L);

		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setFunds(funds);
		paymentOrderDTO.setTargetAccId(targetAcc.getId());

		// Create the PaymentOrder
		restPaymentOrderMockMvc.perform(post("/api/payment-orders/currentuser")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isCreated())
				.andReturn();

		// Validate the PaymentOrder in the database
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate + 1);
		PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
		assertThat(testPaymentOrder.getFunds()).isEqualTo(funds);
		assertThat(testPaymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.ZADAN);
		assertThat(testPaymentOrder.getInsertDate()).isBefore(Instant.now());
		assertThat(testPaymentOrder.getUpdateDate()).isBefore(Instant.now());

		BankAccount sourceAccAfter = bankAccountRepository.findOne(testPaymentOrder.getSourceAcc().getId());
		BankAccount targetAccAfter = bankAccountRepository.findOne(testPaymentOrder.getTargetAcc().getId());

		assertThat(sourceAcc.getFunds()).isEqualTo(sourceAccAfter.getFunds());
		assertThat(targetAcc.getFunds()).isEqualTo(targetAccAfter.getFunds());
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "user", password = "user", roles = "USER")
	public void createPaymentOrderForCurrentUserExceedsFundsShouldReturn4xx() throws Exception {

		Double funds = 1500d;
		BankAccount bankAccount = bankAccountRepository.findOneByUserId(6L);

		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setFunds(funds);
		paymentOrderDTO.setTargetAccId(bankAccount.getId());

		// Create the PaymentOrder
		restPaymentOrderMockMvc.perform(post("/api/payment-orders/currentuser")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isBadRequest())
				.andReturn();
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "user", password = "user", roles = "USER")
	public void createPaymentOrderForCurrentUserNoTargetBankAccShouldReturn4xx() throws Exception {

		Double funds = 200d;

		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setFunds(funds);

		// Create the PaymentOrder
		restPaymentOrderMockMvc.perform(post("/api/payment-orders/currentuser")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isBadRequest())
				.andReturn();
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "user", password = "user", roles = "USER")
	public void createPaymentOrderForCurrentUserFundsLessThan100ShouldBeAutoExecuted() throws Exception {

		Double funds = 99d;

		int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();
		BankAccount sourceAcc = bankAccountRepository.findOneByUserId(4L);
		Double startFundsSource = sourceAcc.getFunds();
		BankAccount targetAcc = bankAccountRepository.findOneByUserId(6L);
		Double startFundsTarget = targetAcc.getFunds();

		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setFunds(funds);
		paymentOrderDTO.setTargetAccId(targetAcc.getId());

		// Create the PaymentOrder
		restPaymentOrderMockMvc.perform(post("/api/payment-orders/currentuser")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isCreated())
				.andReturn();

		// Validate the PaymentOrder in the database
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate + 1);
		PaymentOrder testPaymentOrder = paymentOrderList.get(paymentOrderList.size() - 1);
		assertThat(testPaymentOrder.getFunds()).isEqualTo(funds);
		assertThat(testPaymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.IZVRSEN);

		BankAccount sourceAccAfter = bankAccountRepository.findOne(testPaymentOrder.getSourceAcc().getId());
		BankAccount targetAccAfter = bankAccountRepository.findOne(testPaymentOrder.getTargetAcc().getId());

		assertThat(startFundsSource).isEqualTo(sourceAccAfter.getFunds() + testPaymentOrder.getFunds());
		assertThat(startFundsTarget).isEqualTo(targetAccAfter.getFunds() - testPaymentOrder.getFunds());
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "user", password = "user", roles = "USER")
	public void createPaymentOrderForCurrentUserEvery10thShouldNotBeAutoExecuted() throws Exception {

		Double funds = 99d;

		long startZadanCount = paymentOrderRepository.findAll().stream()
				.filter(x -> x.getStatus().equals(PaymentOrderStatus.ZADAN))
				.count();
		long startIzvrsenCount = paymentOrderRepository.findAll().stream()
				.filter(x -> x.getStatus().equals(PaymentOrderStatus.IZVRSEN))
				.count();
		BankAccount targetAcc = bankAccountRepository.findOneByUserId(6L);

		PaymentOrderDTO paymentOrderDTO = new PaymentOrderDTO();
		paymentOrderDTO.setFunds(funds);
		paymentOrderDTO.setTargetAccId(targetAcc.getId());

		for (int i = 0; i < 11; i++) {
			restPaymentOrderMockMvc.perform(post("/api/payment-orders/currentuser")
					.contentType(TestUtil.APPLICATION_JSON_UTF8)
					.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
					.andExpect(status().isCreated())
					.andReturn();
		}

		long endZadanCount = paymentOrderRepository.findAll().stream()
				.filter(x -> x.getStatus().equals(PaymentOrderStatus.ZADAN))
				.count();
		long endIzvrsenCount = paymentOrderRepository.findAll().stream()
				.filter(x -> x.getStatus().equals(PaymentOrderStatus.IZVRSEN))
				.count();

		assertThat(endIzvrsenCount).isEqualTo(startIzvrsenCount + 10);
		assertThat(endZadanCount).isEqualTo(startZadanCount + 1);
	}

	@Test
	@Transactional
	public void createPaymentOrderWithExistingId() throws Exception {
		int databaseSizeBeforeCreate = paymentOrderRepository.findAll().size();

		// Create the PaymentOrder with an existing ID
		paymentOrder.setId(1L);
		PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

		// An entity with an existing ID cannot be created, so this API call
		// must fail
		restPaymentOrderMockMvc.perform(post("/api/payment-orders")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isBadRequest());

		// Validate the PaymentOrder in the database
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeCreate);
	}

	@Test
	@Transactional
	public void getAllPaymentOrders() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList
		restPaymentOrderMockMvc.perform(get("/api/payment-orders?sort=id,desc"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(paymentOrder.getId().intValue())))
				.andExpect(jsonPath("$.[*].funds").value(hasItem(DEFAULT_FUNDS.doubleValue())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
				.andExpect(jsonPath("$.[*].insertDate").value(hasItem(DEFAULT_INSERT_DATE.toString())))
				.andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())));
	}

	@Test
	@Transactional
	public void getPaymentOrder() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get the paymentOrder
		restPaymentOrderMockMvc.perform(get("/api/payment-orders/{id}", paymentOrder.getId()))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id").value(paymentOrder.getId().intValue()))
				.andExpect(jsonPath("$.funds").value(DEFAULT_FUNDS.doubleValue()))
				.andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
				.andExpect(jsonPath("$.insertDate").value(DEFAULT_INSERT_DATE.toString()))
				.andExpect(jsonPath("$.updateDate").value(DEFAULT_UPDATE_DATE.toString()));
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByFundsIsEqualToSomething() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where funds equals to DEFAULT_FUNDS
		defaultPaymentOrderShouldBeFound("funds.equals=" + DEFAULT_FUNDS);

		// Get all the paymentOrderList where funds equals to UPDATED_FUNDS
		defaultPaymentOrderShouldNotBeFound("funds.equals=" + UPDATED_FUNDS);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByFundsIsInShouldWork() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where funds in DEFAULT_FUNDS or
		// UPDATED_FUNDS
		defaultPaymentOrderShouldBeFound("funds.in=" + DEFAULT_FUNDS + "," + UPDATED_FUNDS);

		// Get all the paymentOrderList where funds equals to UPDATED_FUNDS
		defaultPaymentOrderShouldNotBeFound("funds.in=" + UPDATED_FUNDS);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByFundsIsNullOrNotNull() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where funds is not null
		defaultPaymentOrderShouldBeFound("funds.specified=true");

		// Get all the paymentOrderList where funds is null
		defaultPaymentOrderShouldNotBeFound("funds.specified=false");
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByStatusIsEqualToSomething() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where status equals to DEFAULT_STATUS
		defaultPaymentOrderShouldBeFound("status.equals=" + DEFAULT_STATUS);

		// Get all the paymentOrderList where status equals to UPDATED_STATUS
		defaultPaymentOrderShouldNotBeFound("status.equals=" + UPDATED_STATUS);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByStatusIsInShouldWork() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where status in DEFAULT_STATUS or
		// UPDATED_STATUS
		defaultPaymentOrderShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

		// Get all the paymentOrderList where status equals to UPDATED_STATUS
		defaultPaymentOrderShouldNotBeFound("status.in=" + UPDATED_STATUS);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByStatusIsNullOrNotNull() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where status is not null
		defaultPaymentOrderShouldBeFound("status.specified=true");

		// Get all the paymentOrderList where status is null
		defaultPaymentOrderShouldNotBeFound("status.specified=false");
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByInsertDateIsEqualToSomething() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where insertDate equals to
		// DEFAULT_INSERT_DATE
		defaultPaymentOrderShouldBeFound("insertDate.equals=" + DEFAULT_INSERT_DATE);

		// Get all the paymentOrderList where insertDate equals to
		// UPDATED_INSERT_DATE
		defaultPaymentOrderShouldNotBeFound("insertDate.equals=" + UPDATED_INSERT_DATE);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByInsertDateIsInShouldWork() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where insertDate in DEFAULT_INSERT_DATE
		// or UPDATED_INSERT_DATE
		defaultPaymentOrderShouldBeFound("insertDate.in=" + DEFAULT_INSERT_DATE + "," + UPDATED_INSERT_DATE);

		// Get all the paymentOrderList where insertDate equals to
		// UPDATED_INSERT_DATE
		defaultPaymentOrderShouldNotBeFound("insertDate.in=" + UPDATED_INSERT_DATE);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByUpdateDateIsEqualToSomething() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where updateDate equals to
		// DEFAULT_UPDATE_DATE
		defaultPaymentOrderShouldBeFound("updateDate.equals=" + DEFAULT_UPDATE_DATE);

		// Get all the paymentOrderList where updateDate equals to
		// UPDATED_UPDATE_DATE
		defaultPaymentOrderShouldNotBeFound("updateDate.equals=" + UPDATED_UPDATE_DATE);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByUpdateDateIsInShouldWork() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);

		// Get all the paymentOrderList where updateDate in DEFAULT_UPDATE_DATE
		// or UPDATED_UPDATE_DATE
		defaultPaymentOrderShouldBeFound("updateDate.in=" + DEFAULT_UPDATE_DATE + "," + UPDATED_UPDATE_DATE);

		// Get all the paymentOrderList where updateDate equals to
		// UPDATED_UPDATE_DATE
		defaultPaymentOrderShouldNotBeFound("updateDate.in=" + UPDATED_UPDATE_DATE);
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersBySourceAccIsEqualToSomething() throws Exception {
		// Initialize the database
		BankAccount sourceAcc = BankAccountResourceIntTest.createEntity(em);
		em.persist(sourceAcc);
		em.flush();
		paymentOrder.setSourceAcc(sourceAcc);
		paymentOrderRepository.saveAndFlush(paymentOrder);
		Long sourceAccId = sourceAcc.getId();

		// Get all the paymentOrderList where sourceAcc equals to sourceAccId
		defaultPaymentOrderShouldBeFound("sourceAccId.equals=" + sourceAccId);

		// Get all the paymentOrderList where sourceAcc equals to sourceAccId +
		// 1
		defaultPaymentOrderShouldNotBeFound("sourceAccId.equals=" + (sourceAccId + 1));
	}

	@Test
	@Transactional
	public void getAllPaymentOrdersByTargetAccIsEqualToSomething() throws Exception {
		// Initialize the database
		BankAccount targetAcc = BankAccountResourceIntTest.createEntity(em);
		em.persist(targetAcc);
		em.flush();
		paymentOrder.setTargetAcc(targetAcc);
		paymentOrderRepository.saveAndFlush(paymentOrder);
		Long targetAccId = targetAcc.getId();

		// Get all the paymentOrderList where targetAcc equals to targetAccId
		defaultPaymentOrderShouldBeFound("targetAccId.equals=" + targetAccId);

		// Get all the paymentOrderList where targetAcc equals to targetAccId +
		// 1
		defaultPaymentOrderShouldNotBeFound("targetAccId.equals=" + (targetAccId + 1));
	}

	/**
	 * Executes the search, and checks that the default entity is returned
	 */
	private void defaultPaymentOrderShouldBeFound(String filter) throws Exception {
		restPaymentOrderMockMvc.perform(get("/api/payment-orders?sort=id,desc&" + filter))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.[*].id").value(hasItem(paymentOrder.getId().intValue())))
				.andExpect(jsonPath("$.[*].funds").value(hasItem(DEFAULT_FUNDS.doubleValue())))
				.andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
				.andExpect(jsonPath("$.[*].insertDate").value(hasItem(DEFAULT_INSERT_DATE.toString())))
				.andExpect(jsonPath("$.[*].updateDate").value(hasItem(DEFAULT_UPDATE_DATE.toString())));
	}

	/**
	 * Executes the search, and checks that the default entity is not returned
	 */
	private void defaultPaymentOrderShouldNotBeFound(String filter) throws Exception {
		restPaymentOrderMockMvc.perform(get("/api/payment-orders?sort=id,desc&" + filter))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$").isEmpty());
	}

	@Test
	@Transactional
	public void getNonExistingPaymentOrder() throws Exception {
		// Get the paymentOrder
		restPaymentOrderMockMvc.perform(get("/api/payment-orders/{id}", Long.MAX_VALUE))
				.andExpect(status().isNotFound());
	}

	@Test
	@Transactional
	public void updateNonExistingPaymentOrder() throws Exception {
		int databaseSizeBeforeUpdate = paymentOrderRepository.findAll().size();

		// Create the PaymentOrder
		PaymentOrderDTO paymentOrderDTO = paymentOrderMapper.toDto(paymentOrder);

		// If the entity doesn't have an ID, it will be created instead of just
		// being updated
		restPaymentOrderMockMvc.perform(put("/api/payment-orders")
				.contentType(TestUtil.APPLICATION_JSON_UTF8)
				.content(TestUtil.convertObjectToJsonBytes(paymentOrderDTO)))
				.andExpect(status().isCreated());

		// Validate the PaymentOrder in the database
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeUpdate + 1);
	}

	@Test
	@Transactional
	public void deletePaymentOrder() throws Exception {
		// Initialize the database
		paymentOrderRepository.saveAndFlush(paymentOrder);
		int databaseSizeBeforeDelete = paymentOrderRepository.findAll().size();

		// Get the paymentOrder
		restPaymentOrderMockMvc.perform(delete("/api/payment-orders/{id}", paymentOrder.getId())
				.accept(TestUtil.APPLICATION_JSON_UTF8))
				.andExpect(status().isOk());

		// Validate the database is empty
		List<PaymentOrder> paymentOrderList = paymentOrderRepository.findAll();
		assertThat(paymentOrderList).hasSize(databaseSizeBeforeDelete - 1);
	}

	@Test
	@Transactional
	public void equalsVerifier() throws Exception {
		TestUtil.equalsVerifier(PaymentOrder.class);
		PaymentOrder paymentOrder1 = new PaymentOrder();
		paymentOrder1.setId(1L);
		PaymentOrder paymentOrder2 = new PaymentOrder();
		paymentOrder2.setId(paymentOrder1.getId());
		assertThat(paymentOrder1).isEqualTo(paymentOrder2);
		paymentOrder2.setId(2L);
		assertThat(paymentOrder1).isNotEqualTo(paymentOrder2);
		paymentOrder1.setId(null);
		assertThat(paymentOrder1).isNotEqualTo(paymentOrder2);
	}

	@Test
	@Transactional
	public void dtoEqualsVerifier() throws Exception {
		TestUtil.equalsVerifier(PaymentOrderDTO.class);
		PaymentOrderDTO paymentOrderDTO1 = new PaymentOrderDTO();
		paymentOrderDTO1.setId(1L);
		PaymentOrderDTO paymentOrderDTO2 = new PaymentOrderDTO();
		assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
		paymentOrderDTO2.setId(paymentOrderDTO1.getId());
		assertThat(paymentOrderDTO1).isEqualTo(paymentOrderDTO2);
		paymentOrderDTO2.setId(2L);
		assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
		paymentOrderDTO1.setId(null);
		assertThat(paymentOrderDTO1).isNotEqualTo(paymentOrderDTO2);
	}

	@Test
	@Transactional
	public void testEntityFromId() {
		assertThat(paymentOrderMapper.fromId(42L).getId()).isEqualTo(42);
		assertThat(paymentOrderMapper.fromId(null)).isNull();
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "banka", password = "banka", roles = { "USER", "ADMIN" })
	public void adminCanExecuteOrderIfZadan() throws Exception {
		BankAccount sourceAcc = new BankAccount();
		sourceAcc.setId(1L);
		BankAccount targetAcc = new BankAccount();
		targetAcc.setId(2L);

		PaymentOrder paymentOrder = new PaymentOrder()
				.funds(200d)
				.insertDate(Instant.now())
				.updateDate(Instant.now())
				.sourceAcc(sourceAcc)
				.targetAcc(targetAcc)
				.status(PaymentOrderStatus.ZADAN);
		paymentOrder = paymentOrderRepository.save(paymentOrder);

		restPaymentOrderMockMvc.perform(post("/api/payment-orders/execute/" + paymentOrder.getId()))
				.andExpect(status().isOk())
				.andReturn();

		paymentOrder = paymentOrderRepository.findOne(paymentOrder.getId());
		assertThat(paymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.IZVRSEN);
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "banka", password = "banka", roles = { "USER", "ADMIN" })
	public void adminCanExecuteOrderIfZadanShouldBecomeOdbijenIfNoFunds() throws Exception {
		BankAccount sourceAcc = new BankAccount();
		sourceAcc.setId(1L);
		BankAccount targetAcc = new BankAccount();
		targetAcc.setId(2L);

		PaymentOrder paymentOrder = new PaymentOrder()
				.funds(1500d)
				.insertDate(Instant.now())
				.updateDate(Instant.now())
				.sourceAcc(sourceAcc)
				.targetAcc(targetAcc)
				.status(PaymentOrderStatus.ZADAN);
		paymentOrder = paymentOrderRepository.save(paymentOrder);

		restPaymentOrderMockMvc.perform(post("/api/payment-orders/execute/" + paymentOrder.getId()))
				.andExpect(status().isOk())
				.andReturn();

		paymentOrder = paymentOrderRepository.findOne(paymentOrder.getId());
		assertThat(paymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.ODBIJEN);
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "banka", password = "banka", roles = { "USER", "ADMIN" })
	public void adminCanCancelOrderIfZadan() throws Exception {
		BankAccount sourceAcc = new BankAccount();
		sourceAcc.setId(1L);
		BankAccount targetAcc = new BankAccount();
		targetAcc.setId(2L);

		PaymentOrder paymentOrder = new PaymentOrder()
				.funds(200d)
				.insertDate(Instant.now())
				.updateDate(Instant.now())
				.sourceAcc(sourceAcc)
				.targetAcc(targetAcc)
				.status(PaymentOrderStatus.ZADAN);
		paymentOrder = paymentOrderRepository.save(paymentOrder);

		restPaymentOrderMockMvc.perform(post("/api/payment-orders/cancel/" + paymentOrder.getId()))
				.andExpect(status().isOk())
				.andReturn();

		paymentOrder = paymentOrderRepository.findOne(paymentOrder.getId());
		assertThat(paymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.ODBIJEN);
	}

	// @requirement
	@Test
	@Transactional
	@WithMockUser(username = "banka", password = "banka", roles = { "USER", "ADMIN" })
	public void adminCannotChangeStatusIfNotZadan() throws Exception {
		BankAccount sourceAcc = new BankAccount();
		sourceAcc.setId(1L);
		BankAccount targetAcc = new BankAccount();
		targetAcc.setId(2L);

		PaymentOrder paymentOrder = new PaymentOrder()
				.funds(200d)
				.insertDate(Instant.now())
				.updateDate(Instant.now())
				.sourceAcc(sourceAcc)
				.targetAcc(targetAcc)
				.status(PaymentOrderStatus.ODBIJEN);
		paymentOrder = paymentOrderRepository.save(paymentOrder);

		restPaymentOrderMockMvc.perform(post("/api/payment-orders/execute/" + paymentOrder.getId()))
				.andExpect(status().isOk())
				.andReturn();

		paymentOrder = paymentOrderRepository.findOne(paymentOrder.getId());
		assertThat(paymentOrder.getStatus()).isEqualTo(PaymentOrderStatus.ODBIJEN);
	}
}
