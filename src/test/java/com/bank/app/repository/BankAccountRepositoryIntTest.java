package com.bank.app.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bank.app.BankerApp;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = BankerApp.class)
@Transactional
public class BankAccountRepositoryIntTest {

	@Autowired
	private BankAccountRepository bankAccountRepository;

	@Test
	public void testGetNextAccountNo() {
		assertThat(bankAccountRepository.getNextAccountNo()).isLessThan(bankAccountRepository.getNextAccountNo());
	}

}
