package com.bank.app.domain;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BankAccount.class)
public abstract class BankAccount_ {

	public static volatile SingularAttribute<BankAccount, String> accountNo;
	public static volatile SingularAttribute<BankAccount, Double> funds;
	public static volatile SingularAttribute<BankAccount, Long> id;
	public static volatile SingularAttribute<BankAccount, User> user;

}

