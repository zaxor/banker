package com.bank.app.domain;

import com.bank.app.domain.enumeration.PaymentOrderStatus;
import java.time.Instant;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PaymentOrder.class)
public abstract class PaymentOrder_ {

	public static volatile SingularAttribute<PaymentOrder, Instant> updateDate;
	public static volatile SingularAttribute<PaymentOrder, BankAccount> targetAcc;
	public static volatile SingularAttribute<PaymentOrder, BankAccount> sourceAcc;
	public static volatile SingularAttribute<PaymentOrder, Instant> insertDate;
	public static volatile SingularAttribute<PaymentOrder, Double> funds;
	public static volatile SingularAttribute<PaymentOrder, Long> id;
	public static volatile SingularAttribute<PaymentOrder, Long> version;
	public static volatile SingularAttribute<PaymentOrder, PaymentOrderStatus> status;

}

